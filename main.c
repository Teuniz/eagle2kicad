/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2021 - 2022 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <locale.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <getopt.h>
#include <unistd.h>
#include <signal.h>
#include <stdint.h>
#include <float.h>

#include "global.h"
#include "utils.h"
#include "xml.h"
#include "deviceset.h"
#include "symbol.h"
#include "package.h"


const char k_pcb_layers[][16]=
{
  "",        /* 0 */
  "F.Cu",    /* 1 */
  "In1.Cu",
  "In2.Cu",
  "In3.Cu",
  "In4.Cu",
  "In5.Cu",
  "In6.Cu",
  "In7.Cu",
  "In8.Cu",
  "In9.Cu",
  "In10.Cu",
  "In11.Cu",
  "In12.Cu",
  "In13.Cu",
  "In14.Cu",
  "B.Cu",     /* 16 */
  "F.Adhes",
  "B.Adhes",
  "F.Paste",
  "B.Paste",
  "F.SilkS",
  "B.SilkS",
  "F.Mask",
  "B.Mask",
  "Dwgs.User",   /* 25 */
  "Cmts.User",
  "Eco1.User",
  "Eco2.User",
  "Edge.Cuts",  /* 29 */
  "Margin",
  "F.CrtYd",
  "B.CrtYd",
  "F.Fab",
  "B.Fab",   /* 34 */
};

int glob_use_override_milling_width=0,
    glob_convert_packages=1,
    glob_convert_symbols=1,
    glob_use_dest_dir=0,
    glob_processed_packages=0,
    glob_use_libname_suffix=0,
    glob_use_deviceset_list=0,
    glob_deviceset_list_sz=0,
    glob_verbose=0;

double glob_milling_line_width=0.6,
       glob_restring_multiplier=1.5;

char glob_restrict_layer_name[16]="Dwgs.User",
     glob_milling_layer_name[16]="Edge.Cuts",
     glob_processed_package_names[MAX_PACKAGES][PACK_MAX_DAT_LEN],
     glob_libname_suffix[MAX_LIBNAME_LEN]="",
     glob_deviceset_list_str[MAX_STR_LEN]="",
     glob_deviceset_list[MAX_DEVICESETS_LIST][DEV_MAX_DAT_LEN];

int process_deviceset(struct xml_handle *, FILE *, FILE *, struct deviceset_struct *, struct symbol_struct *, int, int, int, const char *);
void sym_process_arc(double, double, double, double, double, int, int, double, int, FILE *);
void process_package(struct deviceset_struct *, struct package_struct *, const char *, const char *);
void sanitize_string(char *);
int strntoescapeseq(char *, const char *, int);
void get_layer(char *, int, int);
void pack_process_arc(struct pack_wire_struct, const char *, FILE *);
void pack_process_vertex_arc(struct pack_vertex_struct, struct pack_vertex_struct, FILE *);

volatile sig_atomic_t sig_flag=0;

void signal_catch_func(__attribute__((unused)) int sig)
{
  sig_flag = 1;
}


int main(int argc, char **argv)
{
  int i, len, n, err=0,
      deviceset_cnt=0,
      devicesets_processed=0,
      techn_cnt=0,
      dev_cnt=0,
      opt_restring_multiplier_set=0,
      opt_milling_line_width_set=0,
      option_index=0,
      c=0,
      found,
      package_present;

  char str[MAX_STR_LEN]="",
       path_in[MAX_PATH_LEN]="",
       path_out[MAX_PATH_LEN]="",
       lib_name[MAX_PATH_LEN]="",
       dest_dir[MAX_PATH_LEN]="",
       *str_ptr=NULL;

  struct xml_handle *hdl=NULL;

  FILE *fout_lib=NULL,
       *fout_dcm=NULL;

  struct deviceset_struct *deviceset=NULL;

  struct symbol_struct *symbol=NULL;

  struct package_struct *package=NULL;

  setlocale(LC_ALL, "C");

  signal(SIGINT, signal_catch_func);

  setlinebuf(stdout);
  setlinebuf(stderr);

  struct option long_options[] = {
    {"restring", required_argument, 0,  0 },
    {"millinglinewidth", required_argument, 0,  0 },
    {"millinglayer", required_argument, 0,  0 },
    {"skip-packages", no_argument, 0,  0 },
    {"skip-symbols", no_argument, 0,  0 },
    {"dest-dir", required_argument, 0,  0 },
    {"restrict-layer", required_argument, 0,  0 },
    {"libname-suffix", required_argument, 0,  0 },
    {"devicesets", required_argument, 0,  0 },
    {"verbose", no_argument, 0,  0 },
    {"help", no_argument, 0,  0 },
    {0, 0, 0, 0}
  };

  while(1)
  {
    c = getopt_long_only(argc, argv, "", long_options, &option_index);

    if(c == -1)  break;

    if(c == 0)
    {
      if(option_index == 0)
      {
        if(optarg == NULL)
        {
          fprintf(stderr, "missing argument for option %s\n", long_options[option_index].name);
          return EXIT_FAILURE;
        }

        glob_restring_multiplier = atof(optarg);
        opt_restring_multiplier_set = 1;
      }

      if(option_index == 1)
      {
        if(optarg == NULL)
        {
          fprintf(stderr, "missing argument for option %s\n", long_options[option_index].name);
          return EXIT_FAILURE;
        }

        glob_milling_line_width = atof(optarg);
        opt_milling_line_width_set = 1;
        glob_use_override_milling_width = 1;
      }

      if(option_index == 2)
      {
        if(optarg == NULL)
        {
          fprintf(stderr, "missing argument for option %s\n", long_options[option_index].name);
          return EXIT_FAILURE;
        }

        if(strlen(optarg))
        {
          strlcpy(glob_milling_layer_name, optarg, 16);
        }
      }

      if(option_index == 3)
      {
        glob_convert_packages = 0;
      }

      if(option_index == 4)
      {
        glob_convert_symbols = 0;
      }

      if(option_index == 5)
      {
        if(optarg == NULL)
        {
          fprintf(stderr, "missing argument for option %s\n", long_options[option_index].name);
          return EXIT_FAILURE;
        }

        if(strlen(optarg) > 2)
        {
          strlcpy(dest_dir, optarg, MAX_PATH_LEN);
          len = strlen(dest_dir);
          if(dest_dir[len-1] != '/')
          {
            strlcat(dest_dir, "/", MAX_PATH_LEN);
          }
          glob_use_dest_dir = 1;
        }
      }

      if(option_index == 6)
      {
        if(optarg == NULL)
        {
          fprintf(stderr, "missing argument for option %s\n", long_options[option_index].name);
          return EXIT_FAILURE;
        }

        if(strlen(optarg))
        {
          strlcpy(glob_restrict_layer_name, optarg, 16);
        }
      }

      if(option_index == 7)
      {
        if(optarg == NULL)
        {
          fprintf(stderr, "missing argument for option %s\n", long_options[option_index].name);
          return EXIT_FAILURE;
        }

        if(strlen(optarg))
        {
          strlcpy(glob_libname_suffix, optarg, MAX_LIBNAME_LEN);
          sanitize_string(glob_libname_suffix);
          glob_use_libname_suffix = 1;
        }
      }

      if(option_index == 8)
      {
        if(optarg == NULL)
        {
          fprintf(stderr, "missing argument for option %s\n", long_options[option_index].name);
          return EXIT_FAILURE;
        }

        if(strlen(optarg))
        {
          strlcpy(glob_deviceset_list_str, optarg, MAX_STR_LEN);
          glob_use_deviceset_list = 1;
        }
      }

      if(option_index == 9)
      {
        glob_verbose = 1;
      }

      if(option_index == 10)
      {
        fprintf(stdout, "\n Eagle to KiCad library converter\n"
          "\n Copyright (c) 2021 Teunis van Beelen   email: teuniz@protonmail.com\n"
          "\n Usage: eagle2kicad <inputfile.lbr> [OPTION]...\n"
          "\n options:\n"
          "\n --restring=<value>  ratio between pad diameter and drill, must be in the range 2.0 to 1.0, default is 1.5\n"
          "\n --millinglinewidth=<value>  override linewidth in mm for the conversion of the milling layer to the Edge.Cuts layer,\n"
          "                             default is to not override but to take the linewidth from the Eagle library\n"
          "\n --millinglayer=<value>  override the millinglayer, default is Edge.Cuts\n"
          "\n --skip-symbols  do not convert symbols\n"
          "\n --skip-packages  do not convert packages\n"
          "\n --dest-dir=<value>  destination directory for the symbols and footprints (destination directory must exist, it will not be created)\n"
          "\n --restrict-layer=<value>  name of the destination layer for the [tbv]restrict layers, default is Dwgs.User\n"
          "\n --libname-suffix=<value>  add a suffix to the library name e.g. --libname-suffix=-eagle\n"
          "\n --devicesets=<value,...>  convert only specified devicesets (comma separated)\n"
          "\n --verbose\n"
          "\n --help  this screen\n\n"
        );
        return EXIT_SUCCESS;
      }
    }
  }

  if(optind == argc)
  {
    fprintf(stderr, "missing argument inputfile\n--help for help\n");
    return EXIT_FAILURE;
  }

  if(optind < argc)
  {
    strlcpy(path_in, argv[optind++], MAX_PATH_LEN);
  }

  if(optind < argc)
  {
    fprintf(stderr, "unrecognized argument(s):");
    while(optind < argc)
    {
      fprintf(stderr, " %s", argv[optind++]);
    }
    fprintf(stderr, "\n--help for help\n");
    return EXIT_FAILURE;
  }

  if(opt_restring_multiplier_set)
  {
    if(glob_restring_multiplier < 1)
    {
      fprintf(stderr, "*** ERROR *** value for option \"restring\" must be >= 1\n");
      return EXIT_FAILURE;
    }
    if(glob_restring_multiplier > 2)
    {
      fprintf(stderr, "*** ERROR *** value for option \"restring\" must be <= 2\n");
      return EXIT_FAILURE;
    }
  }

  if(opt_milling_line_width_set)
  {
    if(glob_milling_line_width < 0)
    {
      fprintf(stderr, "*** ERROR *** value for option \"millinglinewidth\" must be >= 0\n");
      return EXIT_FAILURE;
    }
    if(glob_milling_line_width > 1)
    {
      fprintf(stderr, "*** ERROR *** value for option \"millinglinewidth\" must be <= 1\n");
      return EXIT_FAILURE;
    }
  }

  for(i=1; i<35; i++)
  {
    if(!strcmp(glob_milling_layer_name, k_pcb_layers[i]))
    {
      break;
    }
  }
  if(i == 35)
  {
    fprintf(stderr, "*** ERROR *** value for option \"restrict-layer\" not recognized: %s\n", glob_milling_layer_name);
    return EXIT_FAILURE;
  }

  for(i=1; i<35; i++)
  {
    if(!strcmp(glob_restrict_layer_name, k_pcb_layers[i]))
    {
      break;
    }
  }
  if(i == 35)
  {
    fprintf(stderr, "*** ERROR *** value for option \"restrict-layer\" not recognized: %s\n", glob_restrict_layer_name);
    return EXIT_FAILURE;
  }

  if(glob_use_deviceset_list)
  {
    for(i=0; i<MAX_DEVICESETS_LIST; i++)
    {
      if(!i)
      {
        str_ptr = strtok(glob_deviceset_list_str, ",");
      }
      else
      {
        str_ptr = strtok(NULL, ",");
      }

      if(str_ptr)
      {
        strlcpy(glob_deviceset_list[i], str_ptr, DEV_MAX_DAT_LEN);
      }
      else
      {
        break;
      }
    }

    glob_deviceset_list_sz = i;

    if(!glob_deviceset_list_sz)
    {
      glob_use_deviceset_list = 0;
    }
  }

  if(strlen(path_in) < 5)
  {
    fprintf(stderr, "*** ERROR *** library filename \"%s\" is too short, must be at least five characters\n", path_in);
    return EXIT_FAILURE;
  }

  if(strcmp(path_in + (strlen(path_in) - 4), ".lbr"))
  {
    fprintf(stderr, "*** ERROR *** library filename has wrong extension, must be \".lbr\"\n");
    return EXIT_FAILURE;
  }

  strlcpy(lib_name, path_in, MAX_PATH_LEN);
  remove_extension_from_filename(lib_name);
  len = strlen(lib_name);
  for(i=len-1, n=-1; i>=0; i--)
  {
    if(lib_name[i] == '/')
    {
      n = i + 1;
      break;
    }
  }
  if(n > 0)
  {
    for(i=0; i<(len-n+1); i++)
    {
      lib_name[i] = lib_name[i + n];
    }
  }
  if(glob_use_libname_suffix)
  {
    strlcat(lib_name, glob_libname_suffix, MAX_PATH_LEN);
  }

  if(glob_use_dest_dir)
  {
    strlcpy(path_out, dest_dir, MAX_PATH_LEN);
    strlcat(path_out, lib_name, MAX_PATH_LEN);
  }
  else
  {
    strlcpy(path_out, path_in, MAX_PATH_LEN);
    remove_extension_from_filename(path_out);
    if(glob_use_libname_suffix)
    {
      strlcat(path_out, glob_libname_suffix, MAX_PATH_LEN);
    }
  }
  strlcat(path_out, ".lib", MAX_PATH_LEN);

  deviceset = calloc(1, sizeof(struct deviceset_struct));
  if(deviceset == NULL)
  {
    fprintf(stderr, "*** ERROR *** cannot allocate memory (deviceset)\n");
    return EXIT_FAILURE;
  }

  symbol = calloc(1, sizeof(struct symbol_struct));
  if(symbol == NULL)
  {
    fprintf(stderr, "*** ERROR *** cannot allocate memory (symbol)\n");
    return EXIT_FAILURE;
  }

  package = calloc(1, sizeof(struct package_struct));
  if(package == NULL)
  {
    fprintf(stderr, "*** ERROR *** cannot allocate memory (package)\n");
    return EXIT_FAILURE;
  }

  hdl = xml_get_handle(path_in);
  if(hdl == NULL)
  {
    fprintf(stderr, "*** ERROR *** cannot open file %s for reading\n", path_in);
    return EXIT_FAILURE;
  }

  if(strcmp(hdl->elementname[hdl->level], "eagle"))
  {
    fprintf(stderr, "*** ERROR *** root element in file %s is not \"eagle\"\n", path_in);
    xml_close(hdl);
    return EXIT_FAILURE;
  }

  if(xml_get_attribute_of_element(hdl, "version", str, MAX_STR_LEN) < 0)
  {
    fprintf(stderr, "*** ERROR *** root element \"eagle\" has no tag \"version\"\n");
    xml_close(hdl);
    return EXIT_FAILURE;
  }

  if((strlen(str) != 5) ||
     (str[1] != '.') ||
     (str[3] != '.') ||
     (atoi(str) < 6) ||
     (atoi(str) > 7))
  {
    fprintf(stderr, "*** ERROR *** version of library is: %s but only V6 and V7 are supported\n", str);
    xml_close(hdl);
    return EXIT_FAILURE;
  }

  if(xml_goto_nth_element_inside(hdl, "drawing", 0))
  {
    fprintf(stderr, "*** ERROR *** cannot find element \"drawing\"\n");
    xml_close(hdl);
    return EXIT_FAILURE;
  }

  if(xml_goto_nth_element_inside(hdl, "library", 0))
  {
    fprintf(stderr, "*** ERROR *** cannot find element \"library\"\n");
    xml_close(hdl);
    return EXIT_FAILURE;
  }

  if(xml_goto_nth_element_inside(hdl, "devicesets", 0))
  {
    fprintf(stderr, "*** ERROR *** cannot find element \"devicesets\"\n");
    xml_close(hdl);
    return EXIT_FAILURE;
  }

  if(glob_convert_symbols)
  {
    fout_lib = fopen(path_out, "wb");
    if(fout_lib == NULL)
    {
      fprintf(stderr, "*** ERROR *** cannot create LIB output file for writing\n");
      xml_close(hdl);
      return EXIT_FAILURE;
    }

    if(fwrite("EESchema-LIBRARY Version 2.4\n#encoding utf-8\n", 45, 1, fout_lib) != 1)
    {
      fprintf(stderr, "*** ERROR *** cannot write to LIB file\n");
      xml_close(hdl);
      fclose(fout_lib);
      return EXIT_FAILURE;
    }

    remove_extension_from_filename(path_out);
    strlcat(path_out, ".dcm", MAX_PATH_LEN);

    fout_dcm = fopen(path_out, "wb");
    if(fout_dcm == NULL)
    {
      fprintf(stderr, "*** ERROR *** cannot create DCM output file for writing\n");
      xml_close(hdl);
      return EXIT_FAILURE;
    }

    if(fwrite("EESchema-DOCLIB  Version 2.0\n#\n", 31, 1, fout_dcm) != 1)
    {
      fprintf(stderr, "*** ERROR *** cannot write to DCM file\n");
      xml_close(hdl);
      fclose(fout_lib);
      fclose(fout_dcm);
      return EXIT_FAILURE;
    }
  }

  for(err=0, deviceset_cnt=0; ; deviceset_cnt++)
  {
    xml_goto_root(hdl);
    xml_goto_nth_element_inside(hdl, "drawing", 0);
    xml_goto_nth_element_inside(hdl, "library", 0);
    xml_goto_nth_element_inside(hdl, "devicesets", 0);

    if(xml_goto_nth_element_inside(hdl, "deviceset", deviceset_cnt))
    {
      break;
    }

    if(glob_use_deviceset_list)
    {
      if(xml_get_attribute_of_element(hdl, "name", str, MAX_STR_LEN) < 0)
      {
        fprintf(stderr, "*** ERROR *** element \"deviceset\" has no attribute \"name\"\n");
        xml_close(hdl);
        return EXIT_FAILURE;
      }

      for(i=0, found=0; i<glob_deviceset_list_sz; i++)
      {
        if(!strcmp(str, glob_deviceset_list[i]))
        {
          found = 1;
          break;
        }
      }

      if(!found)
      {
        xml_go_up(hdl);

        continue;
      }
    }

    err = get_deviceset_data(hdl, deviceset);
    if(err)
    {
      fprintf(stderr, "*** ERROR *** error while getting deviceset data from %s: %i\n", deviceset->name, err);
      break;
    }

    if(glob_verbose)
    {
      fprintf(stdout, "processing deviceset %s\n", deviceset->name);
    }

    for(dev_cnt=0; dev_cnt<deviceset->device_cnt; dev_cnt++)
    {
      for(techn_cnt=0, package_present=0; techn_cnt<deviceset->devices[dev_cnt].technology_cnt; techn_cnt++)
      {
        if(glob_convert_packages)
        {
          if(strlen(deviceset->devices[dev_cnt].package))
          {
            err = get_package_data(hdl, package, deviceset->devices[dev_cnt].package);
            if(err)
            {
              fprintf(stderr, "*** ERROR *** error while getting package data: deviceset: %s   device: %s   package: %s   error: %i\n",
                              deviceset->name, deviceset->devices[dev_cnt].name, deviceset->devices[dev_cnt].package, err);
              break;
            }

            package_present = 1;
          }
        }

        if(glob_convert_symbols)
        {
          err = process_deviceset(hdl, fout_lib, fout_dcm, deviceset, symbol, techn_cnt, dev_cnt, package_present, lib_name);

          if(err)
          {
            fprintf(stderr, "*** ERROR *** error while processing deviceset: %i\n", err);
            break;
          }
        }

        if(glob_convert_packages)
        {
          if(strlen(deviceset->devices[dev_cnt].package))
          {
            process_package(deviceset, package, deviceset->devices[dev_cnt].package, path_out);
          }
        }

        if(sig_flag)  /* ctl-c pressed */
        {
          break;
        }
      }

      if(err)
      {
        break;
      }

      if(sig_flag)  /* ctl-c pressed */
      {
        break;
      }
    }

    if(err)
    {
      break;
    }

    if(sig_flag)  /* ctl-c pressed */
    {
      break;
    }

    xml_go_up(hdl);

    devicesets_processed++;
  }

  xml_go_up(hdl);

  if(glob_verbose)
  {
    fprintf(stdout, "processed %i devicesets\n", devicesets_processed);
  }

  xml_close(hdl);
  hdl = NULL;

  if(sig_flag)  /* ctl-c pressed */
  {
    fprintf(stderr, "\n*** ERROR *** interrupted by user (Ctl-C pressed)\n");
  }

  if(glob_convert_symbols)
  {
    if(fwrite("#\n#End Library\n", 15, 1, fout_lib) != 1)
    {
      fprintf(stderr, "*** ERROR *** cannot write to LIB file\n");
      xml_close(hdl);
      fclose(fout_lib);
      fclose(fout_dcm);
      return EXIT_FAILURE;
    }

    if(fwrite("#End Doc Library\n", 17, 1, fout_dcm) != 1)
    {
      fprintf(stderr, "*** ERROR *** cannot write to DCM file\n");
      xml_close(hdl);
      fclose(fout_lib);
      fclose(fout_dcm);
      return EXIT_FAILURE;
    }
  }

  if(fout_lib != NULL)
  {
    fclose(fout_lib);
  }

  if(fout_dcm != NULL)
  {
    fclose(fout_dcm);
  }

  free(deviceset);
  free(symbol);
  free(package);

  return EXIT_SUCCESS;
}


int process_deviceset(struct xml_handle *hdl, FILE *fout_lib, FILE *fout_dcm, struct deviceset_struct *devset, struct symbol_struct *sym, int techn_cnt, int dev_cnt, int pack_present, const char *libname)
{
  int i, j, k,
      pad_idx=0,
      pinlen,
      x_pos=0,
      y_pos=-250,
      part=1,
      dmg=1,
      pin_name_offset=5,
      dist,
      dash_cnt,
      mirror;

  char pinname[SYM_MAX_DAT_LEN]="",
       symfill='N',
       pinname_vis='Y',
       padname_vis='Y',
       prefixtext_vis='V',
       power_symbol='N',
       sym_text[SYM_MAX_DAT_LEN]="",
       devname[DEV_MAX_DAT_LEN]="",
       packname[DEV_MAX_DAT_LEN]="",
       libname_s[MAX_PATH_LEN]="",
       pad_str[DEV_MAX_DAT_LEN]="",
       *char_ptr=NULL;

  const char pinrot[4]="RULD",
             pintype[9]="BNIOCTPWw",
             text_valign[2][3]={"BCT","TCB"},
             text_halign[2][3]={"LCR","RCL"},
             text_orientation[2]="HV";

  double dx1, dy1, dx2, dy2, dxc, dyc,
         diff_x, diff_y, dash_len_x, dash_len_y;

//  printf("device: %i   technology: %i\n", dev_cnt, techn_cnt);

  strlcpy(devname, devset->name, DEV_MAX_DAT_LEN);

  trim_spaces(devname);

  if(strlen(devname) < 1)
  {
    fprintf(stderr, "*** ERROR *** deviceset has no name (-301)\n");
    return -301;
  }

  if(devset->device_cnt < 1)
  {
    fprintf(stderr, "*** ERROR *** deviceset has no device (-302)\n");
    return -302;
  }

  if(devset->gate_cnt < 1)
  {
    fprintf(stderr, "*** ERROR *** deviceset has no gate (-303)\n");
    return -303;
  }

  if(devset->devices[dev_cnt].technology_cnt < 1)
  {
    fprintf(stderr, "*** ERROR *** device has no technology (-304)\n");
    return -304;
  }

  str_replace_substr(devname, DEV_MAX_DAT_LEN, -1, " ", "_");

  strlcat(devname, devset->devices[dev_cnt].technologies[techn_cnt].name, DEV_MAX_DAT_LEN);

  if(strlen(devset->devices[dev_cnt].name))
  {
    strlcat(devname, "_", DEV_MAX_DAT_LEN);

    strlcat(devname, devset->devices[dev_cnt].name, DEV_MAX_DAT_LEN);
  }

  sanitize_string(devname);

  trim_spaces(devset->prefix);

  if(strlen(devset->prefix) < 1)
  {
    strlcpy(devset->prefix, "~", DEV_MAX_DAT_LEN);
  }
  else
  {
    str_replace_substr(devset->prefix, DEV_MAX_DAT_LEN, -1, " ", "_");

    if(!strcmp(devset->prefix, "IC"))
    {
      strlcpy(devset->prefix, "U", DEV_MAX_DAT_LEN);
    }
  }

  if(glob_verbose)
  {
    fprintf(stdout, "processing symbol %s\n", devset->gates[0].symbol);
  }

  if(get_symbol_data(hdl, sym, devset->gates[0].symbol))
  {
    fprintf(stderr, "*** ERROR *** cannot find symbol \"%s\" (-305)\n", devset->gates[0].symbol);
    return -305;
  }

  pin_name_offset = 5;

  if(sym->pin_cnt > 0)
  {
    if(sym->pins[0].visible == SYM_PIN_VIS_BOTH)
    {
      pinname_vis = 'Y';
      padname_vis = 'Y';
    }
    else if(sym->pins[0].visible == SYM_PIN_VIS_PIN)
      {
        pinname_vis = 'Y';
        padname_vis = 'N';
      }
      else if(sym->pins[0].visible == SYM_PIN_VIS_PAD)
        {
          pinname_vis = 'N';
          padname_vis = 'Y';
        }
        else
        {
          pinname_vis = 'N';
          padname_vis = 'N';
        }

    if(sym->pins[0].direction == SYM_PIN_DIRECT_SUP)
    {
      power_symbol = 'P';
      strlcpy(devset->prefix, "#PWR", DEV_MAX_DAT_LEN);
      pin_name_offset = 0;
      prefixtext_vis = 'I';
    }
  }

  fprintf(fout_dcm, "$CMP %s\nD %s\n$ENDCMP\n#\n", devname, devset->description);

  fprintf(fout_lib, "#\n# %s\n#\n", devname);

  fprintf(fout_lib, "DEF %s %s 0 %i %c %c %i F %c\n", devname, devset->prefix, pin_name_offset, padname_vis, pinname_vis, devset->gate_cnt, power_symbol);

  if(power_symbol != 'P')
  {
    for(i=0; i<sym->text_cnt; i++)
    {
      if((sym->texts[i].layer == E_SCH_NAMES_LAYER) && (!strcmp(sym->texts[i].text, ">NAME")))
      {
        if(sym->texts[i].rotation == SYM_ROT_R180)
        {
          mirror = 1;
        }
        else
        {
          mirror = 0;
        }

        fprintf(fout_lib, "F0 \"%s\" %i %i %i %c %c %c %cNN\n", devset->prefix, mm_to_mill(sym->texts[i].x), mm_to_mill(sym->texts[i].y), mm_to_mill(sym->texts[i].size),
                                                                text_orientation[sym->texts[i].rotation & 1], prefixtext_vis, text_halign[mirror][sym->texts[i].align & 3],
                                                                text_valign[mirror][(sym->texts[i].align >> 2) & 3]);
        break;
      }
    }
    if(i == sym->text_cnt)
    {
      fprintf(fout_lib, "F0 \"%s\" %i %i 50 H V L BNN\n", devset->prefix, x_pos, y_pos);
    }
  }
  else
  {
    fprintf(fout_lib, "F0 \"%s\" 0 -150 50 H I C CNN\n", devset->prefix);
  }

  for(i=0; i<sym->text_cnt; i++)
  {
    if((sym->texts[i].layer == E_SCH_VALUES_LAYER) && (!strcmp(sym->texts[i].text, ">VALUE")))
    {
      if(sym->texts[i].rotation == SYM_ROT_R180)
      {
        mirror = 1;
      }
      else
      {
        mirror = 0;
      }

      fprintf(fout_lib, "F1 \"%s\" %i %i %i %c V %c %cNN\n", devname, mm_to_mill(sym->texts[i].x), mm_to_mill(sym->texts[i].y), mm_to_mill(sym->texts[i].size),
                                                             text_orientation[sym->texts[i].rotation & 1], text_halign[mirror][sym->texts[i].align & 3],
                                                             text_valign[mirror][(sym->texts[i].align >> 2) & 3]);
      break;
    }
  }
  if(i == sym->text_cnt)
  {
    fprintf(fout_lib, "F1 \"%s\" %i %i 50 H V L BNN\n", devname, x_pos, y_pos);
  }

  if(pack_present)
  {
    strlcpy(packname, devset->devices[dev_cnt].package, DEV_MAX_DAT_LEN);

    strlcpy(libname_s, libname, MAX_PATH_LEN);
    sanitize_string(libname_s);
    sanitize_string(packname);

//    printf("libname:packname: ->%s:%s<-\n", libname_s, packname);

      fprintf(fout_lib, "F2 \"%s:%s\" %i %i 50 H I L BNN\n", libname_s, packname, x_pos, y_pos - 200);
  }
  else
  {
    fprintf(fout_lib, "F2 \"\" %i %i 50 H I L BNN\n", x_pos, y_pos - 200);
  }

  fprintf(fout_lib, "F3 \"\" %i %i 50 H V L BNN\n", x_pos, y_pos - 300);

  for(i=0; i<devset->devices[dev_cnt].technologies[techn_cnt].attribute_cnt; i++)
  {
    fprintf(fout_lib, "F%i \"%s\" %i %i 50 H I L BNN \"%s\"\n",
            i + 4, devset->devices[dev_cnt].technologies[techn_cnt].attributes[i].value, x_pos, y_pos - 400 - (i * 100), devset->devices[dev_cnt].technologies[techn_cnt].attributes[i].name);
  }

  fprintf(fout_lib, "DRAW\n");

  for(k=0; k<devset->gate_cnt; k++)
  {
    part = k + 1;

    if(k)
    {
      if(strcmp(devset->gates[k].symbol, devset->gates[k-1].symbol))
      {
        if(glob_verbose)
        {
          fprintf(stdout, "processing symbol %s\n", devset->gates[k].symbol);
        }

        if(get_symbol_data(hdl, sym, devset->gates[k].symbol))
        {
          fprintf(stderr, "*** ERROR *** cannot find symbol \"%s\" (-306)\n", devset->gates[k].symbol);
          return -306;
        }
      }
    }

    for(i=0; i<sym->pin_cnt; i++)
    {
      strlcpy(pinname, sym->pins[i].name, SYM_MAX_DAT_LEN);
      if(pinname[0] == '!')
      {
        pinname[0] = '~';
      }

      pinlen = (sym->pins[i].length - 1) * 100;
      if(pinlen < 0)
      {
        pinlen = 300;
      }

      for(pad_idx=0; pad_idx<devset->devices[dev_cnt].connect_cnt; pad_idx++)
      {
        if(!strcmp(devset->devices[dev_cnt].connects[pad_idx].pin, sym->pins[i].name))
        {
          if(!strcmp(devset->devices[dev_cnt].connects[pad_idx].gate, devset->gates[k].name))
          {
            break;
          }
        }
      }
      if(pad_idx == devset->devices[dev_cnt].connect_cnt)
      {
        if(power_symbol == 'P')
        {
          fprintf(fout_lib, "X %s 1 %i %i %i %c 50 50 %i %i W\n", pinname, mm_to_mill(sym->pins[i].x), mm_to_mill(sym->pins[i].y),
                                                                pinlen, pinrot[sym->pins[i].rotation % 4], part, dmg);
        }
        else
        {
          fprintf(fout_lib, "X %s ~ %i %i %i %c 50 50 %i %i %c\n", pinname, mm_to_mill(sym->pins[i].x), mm_to_mill(sym->pins[i].y),
                                                                pinlen, pinrot[sym->pins[i].rotation % 4], part, dmg, pintype[sym->pins[i].direction % 9]);
        }
      }
      else
      {
        strlcpy(pad_str, devset->devices[dev_cnt].connects[pad_idx].pad, DEV_MAX_DAT_LEN);

        char_ptr = strtok(pad_str, " ");
        while(char_ptr != NULL)
        {
          fprintf(fout_lib, "X %s %s %i %i %i %c 50 50 %i %i %c\n", pinname, char_ptr, mm_to_mill(sym->pins[i].x), mm_to_mill(sym->pins[i].y),
                                                                pinlen, pinrot[sym->pins[i].rotation % 4], part, dmg, pintype[sym->pins[i].direction % 9]);

          char_ptr = strtok(NULL, " ");

          if(sig_flag)  /* ctl-c pressed */
          {
            break;
          }
        }
      }
    }

    for(i=0; i<sym->wire_cnt; i++)
    {
      if(sym->wires[i].layer == E_SCH_SYMBOLS_LAYER)
      {
        if(((sym->wires[i].curve < 0.1) && (sym->wires[i].curve > -0.1)) || (rect_dist(sym->wires[i].x1, sym->wires[i].y1, sym->wires[i].x2, sym->wires[i].y2) < 0.1))
        {
          if(sym->wires[i].style == SYM_WIRE_STYLE_CONTINUOUS)
          {
            fprintf(fout_lib, "P 2 %i %i %i %i %i %i %i N\n", part, dmg, mm_to_mill(sym->wires[i].width), mm_to_mill(sym->wires[i].x1), mm_to_mill(sym->wires[i].y1),
                                                          mm_to_mill(sym->wires[i].x2), mm_to_mill(sym->wires[i].y2));
          }
          else
          {
            dist = mm_to_mill(rect_dist(sym->wires[i].x1, sym->wires[i].y1, sym->wires[i].x2, sym->wires[i].y2));

            dash_cnt = dist / 80;
            if(dash_cnt < 1)
            {
              fprintf(fout_lib, "P 2 %i %i %i %i %i %i %i N\n", part, dmg, mm_to_mill(sym->wires[i].width), mm_to_mill(sym->wires[i].x1), mm_to_mill(sym->wires[i].y1),
                                                            mm_to_mill(sym->wires[i].x2), mm_to_mill(sym->wires[i].y2));
            }
            else
            {
              diff_x = sym->wires[i].x2 - sym->wires[i].x1;

              diff_y = sym->wires[i].y2 - sym->wires[i].y1;

              dash_len_x = diff_x / ((dash_cnt * 2) + 1);

              dash_len_y = diff_y / ((dash_cnt * 2) + 1);

              for(j=0; j<(dash_cnt + 1); j++)
              {
                fprintf(fout_lib, "P 2 %i %i %i %i %i %i %i N\n",
                        part, dmg, mm_to_mill(sym->wires[i].width),
                        mm_to_mill(sym->wires[i].x1 + (j * dash_len_x * 2)),
                        mm_to_mill(sym->wires[i].y1 + (j * dash_len_y * 2)),
                        mm_to_mill(sym->wires[i].x1 + (j * dash_len_x * 2) + dash_len_x),
                        mm_to_mill(sym->wires[i].y1 + (j * dash_len_y * 2) + dash_len_y));
              }
            }
          }
        }
        else
        {
          if((sym->wires[i].curve < -359.900001) || (sym->wires[i].curve > 359.900001))
          {
            fprintf(stderr, "*** ERROR *** curve of wire in symbol \"%s\" is out of range: %.8f degrees (-307)\n", sym->name, sym->wires[i].curve);
            return -307;
          }

          sym_process_arc(sym->wires[i].x1, sym->wires[i].y1, sym->wires[i].x2, sym->wires[i].y2,
                          sym->wires[i].curve, part, dmg, sym->wires[i].width, 0, fout_lib);
        }
      }
    }

    for(i=0; i<sym->polygon_cnt; i++)
    {
      if(sym->polygons[i].layer == E_SCH_SYMBOLS_LAYER)
      {
        if(sym->polygons[i].vertex_cnt < 1)
        {
          fprintf(stderr, "*** ERROR *** symbol \"%s\" has a polygon without vertices (-308)\n", devset->gates[k].symbol);
          return -308;
        }

        fprintf(fout_lib, "P %i %i %i %i", sym->polygons[i].vertex_cnt + 1, part, dmg, mm_to_mill(sym->wires[i].width));

        for(j=0; j<sym->polygons[i].vertex_cnt; j++)
        {
          fprintf(fout_lib, " %i %i", mm_to_mill(sym->polygons[i].vertices[j].x), mm_to_mill(sym->polygons[i].vertices[j].y));
        }

        fprintf(fout_lib, " %i %i F\n", mm_to_mill(sym->polygons[i].vertices[0].x), mm_to_mill(sym->polygons[i].vertices[0].y));

        for(j=0; j<sym->polygons[i].vertex_cnt; j++)
        {
          if(dblcmp(sym->polygons[i].vertices[j].curve, 0))
          {
            if((sym->polygons[i].vertices[j].curve < -359.900001) || (sym->polygons[i].vertices[j].curve > 359.900001))
            {
              fprintf(stderr, "*** ERROR *** curve of vertex in symbol \"%s\" is out of range: %.8f degrees (-309)\n", sym->name, sym->polygons[i].vertices[j].curve);
              return -309;
            }

            sym_process_arc(sym->polygons[i].vertices[j].x, sym->polygons[i].vertices[j].y,
                            sym->polygons[i].vertices[(j + 1) % sym->polygons[i].vertex_cnt].x, sym->polygons[i].vertices[(j + 1) % sym->polygons[i].vertex_cnt].y,
                            sym->polygons[i].vertices[j].curve, part, dmg, sym->polygons[i].width, 1, fout_lib);
          }
        }
      }
    }

    for(i=0; i<sym->text_cnt; i++)
    {
      if(sym->texts[i].layer == E_SCH_SYMBOLS_LAYER)
      {
        strlcpy(sym_text, sym->texts[i].text, SYM_MAX_DAT_LEN);

        str_replace_substr(sym_text, SYM_MAX_DAT_LEN, -1, " ", "~");

        str_replace_substr(sym_text, SYM_MAX_DAT_LEN, -1, "\n", "~");

        if(strlen(sym_text) < 1)
        {
          strlcpy(sym_text, "~", SYM_MAX_DAT_LEN);
        }

        fprintf(fout_lib, "T %i %i %i %i 0 %i %i %s Normal 0 %c %c\n", (sym->texts[i].rotation & 1) * 900, mm_to_mill(sym->texts[i].x), mm_to_mill(sym->texts[i].y), mm_to_mill(sym->texts[i].size),
                                                                   part, dmg, sym_text, text_halign[sym->texts[i].rotation >> 1][sym->texts[i].align & 3],
                                                                   text_valign[sym->texts[i].rotation >> 1][(sym->texts[i].align >> 2) & 3]);
      }
    }

    for(i=0; i<sym->circle_cnt; i++)
    {
      if(sym->circles[i].layer == E_SCH_SYMBOLS_LAYER)
      {
        if(mm_to_mill(sym->circles[i].width) < 1)
        {
          symfill = 'F';
        }
        else
        {
          symfill = 'N';
        }

        fprintf(fout_lib, "C %i %i %i %i %i %i %c\n", mm_to_mill(sym->circles[i].x), mm_to_mill(sym->circles[i].y), mm_to_mill(sym->circles[i].radius), part, dmg, mm_to_mill(sym->circles[i].width), symfill);
      }
    }

    for(i=0; i<sym->rectangle_cnt; i++)
    {
      if(sym->rectangles[i].layer == E_SCH_SYMBOLS_LAYER)
      {
        dx1 = sym->rectangles[i].x1;
        dy1 = sym->rectangles[i].y1;
        dx2 = sym->rectangles[i].x2;
        dy2 = sym->rectangles[i].y2;

        if((sym->rectangles[i].rotation == SYM_ROT_R90) || (sym->rectangles[i].rotation == SYM_ROT_R270))
        {
          dxc = dx2 + ((dx1 - dx2) / 2);
          dyc = dy2 + ((dy1 - dy2) / 2);

          dx1 -= dxc;
          dy1 -= dyc;
          rect_rotate(&dx1, &dy1, sym->rectangles[i].rotation * 90);
          dx1 += dxc;
          dy1 += dyc;

          dx2 -= dxc;
          dy2 -= dyc;
          rect_rotate(&dx2, &dy2, sym->rectangles[i].rotation * 90);
          dx2 += dxc;
          dy2 += dyc;
        }

        fprintf(fout_lib, "S %i %i %i %i %i %i F\n", mm_to_mill(dx1), mm_to_mill(dy1), mm_to_mill(dx2), mm_to_mill(dy2), part, dmg);
      }
    }

    if(sig_flag)  /* ctl-c pressed */
    {
      break;
    }
  }

  fprintf(fout_lib, "ENDDRAW\nENDDEF\n");

  return 0;
}


void process_package(__attribute__((unused)) struct deviceset_struct *devset, struct package_struct *pack, __attribute__((unused)) const char *pack__name, const char *path)
{
  int i, j;

  char str[MAX_STR_LEN]="",
       mod_dir[MAX_PATH_LEN]="",
       mod_name[MAX_PATH_LEN]="",
       pack_name[PACK_MAX_DAT_LEN]="",
       devname[DEV_MAX_DAT_LEN]="",
       fb='F';

  double sz_x=0, sz_y=0, width=0;

  FILE * f_out=NULL;

  strlcpy(devname, devset->name, DEV_MAX_DAT_LEN);

  trim_spaces(devname);

  if(strlen(devname) < 1)
  {
    fprintf(stderr, "*** ERROR *** deviceset has no name (-1001)\n");
    return;
  }

  strlcpy(pack_name, pack->name, PACK_MAX_DAT_LEN);
  sanitize_string(pack_name);

  strlcpy(mod_dir, path, MAX_PATH_LEN);
  remove_extension_from_filename(mod_dir);
  strlcat(mod_dir, ".pretty", MAX_PATH_LEN);

  strlcpy(mod_name, mod_dir, MAX_PATH_LEN);
  strlcat(mod_name, "/", MAX_PATH_LEN);
  strlcat(mod_name, pack_name, MAX_PATH_LEN);
  strlcat(mod_name, ".kicad_mod", MAX_PATH_LEN);

  for(i=0; i<glob_processed_packages; i++)
  {
    if(!strcmp(glob_processed_package_names[i], pack_name))
    {
      return;
    }
  }

  if(glob_verbose)
  {
    fprintf(stdout, "processing package %s\n", pack->name);
  }

  f_out = fopen(mod_name, "wb");
  if(f_out == NULL)
  {
    if(mkdir(mod_dir, 0755))
    {
      printf("*** ERROR *** can not create directory %s for %s\n", mod_dir, mod_name);
      return;
    }

    f_out = fopen(mod_name, "wb");
    if(f_out == NULL)
    {
      printf("*** ERROR *** can not create file %s\n", mod_name);
      return;
    }
  }

  fprintf(f_out, "(module %s (layer F.Cu) (tedit %lX)\n", pack_name, time(NULL));

  if(strlen(pack->description))
  {
    strntoescapeseq(str, pack->description, MAX_STR_LEN);
    fprintf(f_out, "  (descr \"%s\")\n", str);
  }

  for(i=0; i<pack->text_cnt; i++)
  {
    if(!strcmp(pack->texts[i].text, ">NAME"))
    {
      if((pack->texts[i].layer == E_PCB_TNAMES_LAYER) || (pack->texts[i].layer == E_PCB_BNAMES_LAYER))
      {
        if(pack->texts[i].layer == E_PCB_TNAMES_LAYER)
        {
          fb = 'F';
        }
        else
        {
          fb = 'B';
        }

        fprintf(f_out, "  (fp_text reference REF** (at %.3f %.3f %.1f) (layer %c.SilkS) (effects (font (size %.3f %.3f))))\n",
                       pack->texts[i].x, -pack->texts[i].y, pack->texts[i].rotation, fb, pack->texts[i].size, pack->texts[i].size);
        break;
      }
    }
  }

  for(i=0; i<pack->text_cnt; i++)
  {
    if(!strcmp(pack->texts[i].text, ">VALUE"))
    {
      if((pack->texts[i].layer == E_PCB_TVALUES_LAYER) || (pack->texts[i].layer == E_PCB_BVALUES_LAYER))
      {
        if(pack->texts[i].layer == E_PCB_TVALUES_LAYER)
        {
          fb = 'F';
        }
        else
        {
          fb = 'B';
        }

        strntoescapeseq(str, devname, MAX_STR_LEN);

        fprintf(f_out, "  (fp_text value \"%s\" (at %.3f %.3f %.1f) (layer %c.Fab) (effects (font (size %.3f %.3f))))\n",
                       str, pack->texts[i].x, -pack->texts[i].y, pack->texts[i].rotation, fb, pack->texts[i].size, pack->texts[i].size);
        break;
      }
    }
  }

  for(i=0; i<pack->wire_cnt; i++)
  {
    if(pack->wires[i].layer == E_PCB_MILLING_LAYER)
    {
      strlcpy(str, glob_milling_layer_name, MAX_STR_LEN);
    }
    else
    {
      get_layer(str, pack->wires[i].layer, MAX_STR_LEN);
    }

    if(!dblcmp(pack->wires[i].curve, 0))
    {
      if((pack->wires[i].layer == E_PCB_MILLING_LAYER) && glob_use_override_milling_width)
      {
        width = glob_milling_line_width;
      }
      else
      {
        width = pack->wires[i].width;
      }

      fprintf(f_out, "  (fp_line (start %.3f %.3f) (end %.3f %.3f) (layer %s) (width %.3f))\n",
                     pack->wires[i].x1, -pack->wires[i].y1, pack->wires[i].x2, -pack->wires[i].y2,
                     str, width);
    }
    else
    {
      if((pack->wires[i].curve < -359.900001) || (pack->wires[i].curve > 359.900001))
      {
        fprintf(stderr, "*** ERROR *** curve of wire in symbol \"%s\" is out of range: %.8f degrees (-307)\n", pack->name, pack->wires[i].curve);
        continue;
      }

      pack_process_arc(pack->wires[i], str, f_out);
    }
  }

  int before;

  for(i=0; i<pack->polygon_cnt; i++)
  {
    if(!pack->polygons[i].vertex_cnt)  continue;

    get_layer(str, pack->polygons[i].layer, MAX_STR_LEN);

    fprintf(f_out, "  (fp_poly (pts");

    for(j=0; j<pack->polygons[i].vertex_cnt; j++)
    {
      before = (j + pack->polygons[i].vertex_cnt - 1) % pack->polygons[i].vertex_cnt;

      if(!dblcmp(pack->polygons[i].vertices[before].curve, 0))
      {
        fprintf(f_out, " (xy %.3f %.3f)", pack->polygons[i].vertices[j].x, -pack->polygons[i].vertices[j].y);
      }
      else
      {
        if((pack->polygons[i].vertices[before].curve < -359.900001) || (pack->polygons[i].vertices[before].curve > 359.900001))
        {
          fprintf(stderr, "*** ERROR *** curve of wire in symbol \"%s\" is out of range: %.8f degrees (-307)\n", pack->name, pack->polygons[i].vertices[before].curve);
          continue;
        }

        pack_process_vertex_arc(pack->polygons[i].vertices[before], pack->polygons[i].vertices[j], f_out);
      }
    }

    fprintf(f_out, ") (layer %s) (width %.3f))\n", str, pack->polygons[i].width);
  }

  for(i=0; i<pack->circle_cnt; i++)
  {
    if(pack->circles[i].layer == E_PCB_MILLING_LAYER)
    {
      strlcpy(str, glob_milling_layer_name, MAX_STR_LEN);
    }
    else
    {
      get_layer(str, pack->circles[i].layer, MAX_STR_LEN);
    }

    if((pack->circles[i].layer == E_PCB_MILLING_LAYER) && glob_use_override_milling_width)
    {
      width = glob_milling_line_width;
    }
    else
    {
      width = pack->circles[i].width;
    }

    fprintf(f_out, "  (fp_circle (center %.3f %.3f) (end %.3f %.3f) (layer %s) (width %.3f))\n",
                   pack->circles[i].x, -pack->circles[i].y, pack->circles[i].x + pack->circles[i].radius, -pack->circles[i].y,
                   str, width);
  }

  for(i=0; i<pack->hole_cnt; i++)
  {
    sz_x = pack->holes[i].drill + 1;
    sz_y = sz_x;

    fprintf(f_out, "  (pad \"\" np_thru_hole circle (at %.3f %.3f 0) (size %.3f %.3f) (drill %.3f) (layers *.Cu *.Mask))\n",
                   pack->holes[i].x, -pack->holes[i].y, sz_x, sz_y, pack->holes[i].drill);
  }

  for(i=0; i<pack->smd_cnt; i++)
  {
    if(pack->smds[i].layer == E_PCB_TOP_LAYER)
    {
      fb = 'F';
    }
    else
    {
      fb = 'B';
    }

    if(pack->smds[i].roundness)
    {
      fprintf(f_out, "  (pad \"%s\" smd roundrect (at %.3f %.3f %.3f) (size %.3f %.3f) (layers %c.Cu %c.Mask %c.Paste) (roundrect_rratio %.3f))\n",
                     pack->smds[i].name, pack->smds[i].x, -pack->smds[i].y, pack->smds[i].rotation, pack->smds[i].dx, pack->smds[i].dy, fb, fb, fb, pack->smds[i].roundness / 2.0);
    }
    else
    {
      fprintf(f_out, "  (pad \"%s\" smd rect (at %.3f %.3f %.3f) (size %.3f %.3f) (layers %c.Cu %c.Mask %c.Paste))\n",
                     pack->smds[i].name, pack->smds[i].x, -pack->smds[i].y, pack->smds[i].rotation, pack->smds[i].dx, pack->smds[i].dy, fb, fb, fb);
    }
  }

  for(i=0; i<pack->pad_cnt; i++)
  {
    if(pack->pads[i].diameter > 0.001)
    {
      sz_x = pack->pads[i].diameter;
    }
    else
    {
      sz_x = pack->pads[i].drill * glob_restring_multiplier;
    }
    sz_y = sz_x;

    fprintf(f_out, "  (pad \"%s\" thru_hole circle (at %.3f %.3f 0) (size %.3f %.3f) (drill %.3f) (layers *.Cu *.Mask))\n",
                   pack->pads[i].name, pack->pads[i].x, -pack->pads[i].y, sz_x, sz_y, pack->pads[i].drill);
  }

  fprintf(f_out, "  (fp_text user %%R (at 0 0) (layer F.Fab) (effects (font (size 0.8 0.8))))\n");

  fprintf(f_out, ")\n");

  if(f_out != NULL)
  {
    fclose(f_out);
  }

  if(glob_processed_packages < MAX_PACKAGES)
  {
    strlcpy(glob_processed_package_names[glob_processed_packages++], pack_name, PACK_MAX_DAT_LEN);
  }
}


void get_layer(char *str, int layer, int len)
{
  if(layer <= E_PCB_BOTTOM_LAYER)
  {
    strlcpy(str, k_pcb_layers[layer], len);
  }
  else if(layer == E_PCB_TPLACE_LAYER)
    {
      strlcpy(str, "F.SilkS", len);
    }
    else if(layer == E_PCB_BPLACE_LAYER)
      {
        strlcpy(str, "B.SilkS", len);
      }
      else if(layer == E_PCB_TSTOP_LAYER)
        {
          strlcpy(str, "F.Mask", len);
        }
        else if(layer == E_PCB_BSTOP_LAYER)
          {
            strlcpy(str, "B.Mask", len);
          }
          else if(layer == E_PCB_TKEEPOUT_LAYER)
            {
              strlcpy(str, "F.CrtYd", len);
            }
            else if(layer == E_PCB_BKEEPOUT_LAYER)
              {
                strlcpy(str, "B.CrtYd", len);
              }
              else if(layer == E_PCB_MILLING_LAYER)
                {
                  strlcpy(str, "Edge.Cuts", len);
                }
                else if((layer == E_PCB_TRESTRICT_LAYER) || (layer == E_PCB_BRESTRICT_LAYER) || (layer == E_PCB_VRESTRICT_LAYER))
                  {
                    strlcpy(str, glob_restrict_layer_name, len);
                  }
                  else
                  {
                    strlcpy(str, "Eco2.User", len);
                  }
}


void pack_process_vertex_arc(struct pack_vertex_struct vertex1, struct pack_vertex_struct vertex2, FILE *fout)
{
  int i, steps;

  double x, y, curve_part, d;

  struct arc_param_struct arc_p;

  d = hypot(vertex2.x - vertex1.x, vertex2.y - vertex1.y);

  if(d < 0.05)
  {
    fprintf(fout, " (xy %.3f %.3f)", vertex2.x, -vertex2.y);

    return;
  }

  steps = (d * fabs(vertex1.curve)) / 18;

//  printf("steps: %i    distance: %f\n", steps, d);

  if(steps < 2) steps = 2;

  arc_p.x1 = vertex1.x;
  arc_p.y1 = vertex1.y;
  arc_p.x2 = vertex2.x;
  arc_p.y2 = vertex2.y;
  arc_p.curve = vertex1.curve;

  get_arc_from_curve_endpoints(&arc_p);

  curve_part = vertex1.curve / steps;

//  printf(" (xy %.3f %.3f)", vertex1.x, -vertex1.y);

  for(i=1; i<steps; i++)
  {
//    printf(" curve: %.1f", curve_part * i);

    polar_to_rect(arc_p.radius, arc_p.start + (curve_part * i), &x, &y);

    x += arc_p.xc;
    y += arc_p.yc;

    fprintf(fout, " (xy %.3f %.3f)", x, -y);

//    printf(" (xy %.3f %.3f)", x, -y);
  }

  fprintf(fout, " (xy %.3f %.3f)", vertex2.x, -vertex2.y);

//  printf(" (xy %.3f %.3f)\n", vertex2.x, -vertex2.y);
}


void pack_process_arc(struct pack_wire_struct wire, const char *layer, FILE *fout)
{
  double width;

  struct arc_param_struct arc_p;

  arc_p.x1 = wire.x1;
  arc_p.y1 = wire.y1;
  arc_p.x2 = wire.x2;
  arc_p.y2 = wire.y2;
  arc_p.curve = wire.curve;

  get_arc_from_curve_endpoints(&arc_p);

//   printf("==============\n"
//          "curve: %.3f   x1: %.3f   y1: %.3f   x2: %.3f   y2: %.3f   xc: %.3f   yc: %.3f\n",
//          wire.curve, wire.x1, wire.y1, wire.x2, wire.y2, arc_p.xc, arc_p.yc);

//   if(fabs(arc_p.curve) > 179.9)
//   {
//     polar_to_rect(arc_p.radius, arc_p.start + (arc_p.curve / 2), &arc_p.x3, &arc_p.y3);
//     arc_p.x3 += arc_p.xc;
//     arc_p.y3 += arc_p.yc;
//
// //   printf("angle start: %.3f   angle end: %.3f   x3: %.3f   y3: %.3f\n",
// //          arc_p.start, arc_p.end, arc_p.x3, arc_p.y3);
//
//     fprintf(fout, "  (fp_arc (start %.3f %.3f) (end %.3f %.3f) (angle %.3f) (layer %s) (width %.3f))\n",
//                   arc_p.xc, arc_p.yc, wire.x1, wire.y1, wire.curve / 2, layer, wire.width);
//
//     fprintf(fout, "  (fp_arc (start %.3f %.3f) (end %.3f %.3f) (angle %.3f) (layer %s) (width %.3f))\n",
//                   arc_p.xc, arc_p.yc, arc_p.x3, arc_p.y3, wire.curve / 2, layer, wire.width);
//
// //     fprintf(fout, "A %i %i %i %i %i %i %i %i %c %i %i %i %i\n", mm_to_mill(arc_p.xc), mm_to_mill(arc_p.yc), mm_to_mill(arc_p.radius),
// //                                                                (int)((arc_p.start * 10) + 0.5), (int)(((arc_p.end - (arc_p.curve / 2)) * 10) + 0.5),
// //                                                                part, dmg, mm_to_mill(width), f, mm_to_mill(x1),
// //                                                                mm_to_mill(y1), mm_to_mill(arc_p.x3), mm_to_mill(arc_p.y3));
// //
// //
// //     fprintf(fout, "A %i %i %i %i %i %i %i %i %c %i %i %i %i\n", mm_to_mill(arc_p.xc), mm_to_mill(arc_p.yc), mm_to_mill(arc_p.radius),
// //                                                                (int)(((arc_p.start + (arc_p.curve / 2)) * 10) + 0.5), (int)((arc_p.end * 10) + 0.5),
// //                                                                part, dmg, mm_to_mill(width), f, mm_to_mill(arc_p.x3),
// //                                                                mm_to_mill(arc_p.y3), mm_to_mill(x2), mm_to_mill(y2));
//   }
//   else
//   {
    if((wire.layer == E_PCB_MILLING_LAYER) && glob_use_override_milling_width)
    {
      width = glob_milling_line_width;
    }
    else
    {
      width = wire.width;
    }

    fprintf(fout, "  (fp_arc (start %.3f %.3f) (end %.3f %.3f) (angle %.3f) (layer %s) (width %.3f))\n",
                  arc_p.xc, -arc_p.yc, wire.x1, -wire.y1, -wire.curve, layer, width);
//   }
}


void sym_process_arc(double x1, double y1, double x2, double y2, double curve, int part, int dmg, double width, int fill, FILE *fout)
{
  char f;

  struct arc_param_struct arc_p;

  if(fill)
  {
    f = 'F';
  }
  else
  {
    f = 'N';
  }

  arc_p.x1 = x1;
  arc_p.y1 = y1;
  arc_p.x2 = x2;
  arc_p.y2 = y2;
  arc_p.curve = curve;

  get_arc_from_curve_endpoints(&arc_p);

  if(fabs(arc_p.curve) > 179.9)
  {
    polar_to_rect(arc_p.radius, arc_p.start + (arc_p.curve / 2), &arc_p.x3, &arc_p.y3);
    arc_p.x3 += arc_p.xc;
    arc_p.y3 += arc_p.yc;

    fprintf(fout, "A %i %i %i %i %i %i %i %i %c %i %i %i %i\n", mm_to_mill(arc_p.xc), mm_to_mill(arc_p.yc), mm_to_mill(arc_p.radius),
                                                               (int)((arc_p.start * 10) + 0.5), (int)(((arc_p.end - (arc_p.curve / 2)) * 10) + 0.5),
                                                               part, dmg, mm_to_mill(width), f, mm_to_mill(x1),
                                                               mm_to_mill(y1), mm_to_mill(arc_p.x3), mm_to_mill(arc_p.y3));


    fprintf(fout, "A %i %i %i %i %i %i %i %i %c %i %i %i %i\n", mm_to_mill(arc_p.xc), mm_to_mill(arc_p.yc), mm_to_mill(arc_p.radius),
                                                               (int)(((arc_p.start + (arc_p.curve / 2)) * 10) + 0.5), (int)((arc_p.end * 10) + 0.5),
                                                               part, dmg, mm_to_mill(width), f, mm_to_mill(arc_p.x3),
                                                               mm_to_mill(arc_p.y3), mm_to_mill(x2), mm_to_mill(y2));
  }
  else
  {
    fprintf(fout, "A %i %i %i %i %i %i %i %i %c %i %i %i %i\n", mm_to_mill(arc_p.xc), mm_to_mill(arc_p.yc), mm_to_mill(arc_p.radius),
                                                               (int)((arc_p.start * 10) + 0.5), (int)((arc_p.end * 10) + 0.5),
                                                               part, dmg, mm_to_mill(width), f, mm_to_mill(x1),
                                                               mm_to_mill(y1), mm_to_mill(x2), mm_to_mill(y2));
  }
}


void sanitize_string(char *s)
{
  for(; *s; s++)
  {
    if((*s < 'A') || (*s > 'Z'))
    {
      if((*s < 'a') || (*s > 'z'))
      {
        if((*s < '0') || (*s > '9'))
        {
          if((*s != '_') && (*s != '-') && (*s != '.') && (*s != ',') && (*s != '+'))
          {
            *s = '_';
          }
        }
      }
    }
  }
}


int strntoescapeseq(char *dest, const char *src, int destlen)
{
  int i=0, j, srclen;

  srclen = strlen(src);

  for(j=0; j<srclen; j++)
  {
    if((destlen - i) < 3)  break;

    if(src[j] == '\n')
    {
      dest[i++] = '\\';
      dest[i++] = 'n';
    }
    else if(src[j] == '\t')
      {
        dest[i++] = '\\';
        dest[i++] = 't';
      }
      else if(src[j] == '\"')
        {
          dest[i++] = '\\';
          dest[i++] = '\"';
        }
        else if((src[j] >= 0) && (src[j] < 32))
          {
            dest[i++] = ' ';
          }
          else
          {
            dest[i++] = src[j];
          }
  }

  dest[i] = 0;

  return i;
}













