/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2021 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/


#ifndef deviceset_INCLUDED
#define deviceset_INCLUDED

#include "global.h"
#include "utils.h"
#include "xml.h"



#define DEV_MAX_DAT_LEN        (256)

#define DEV_MAX_GATES          (128)

#define DEV_MAX_CONNECTS      (1024)

#define DEV_MAX_DEVICES         (16)

#define DEV_MAX_TECHNOLOGIES    (16)

#define DEV_MAX_ATTRIBUTES      (64)



struct attribute_struct
{
  char name[DEV_MAX_DAT_LEN];
  char value[DEV_MAX_DAT_LEN];
};


struct technology_struct
{
  char name[DEV_MAX_DAT_LEN];
  int attribute_cnt;
  struct attribute_struct attributes[DEV_MAX_ATTRIBUTES];
};


struct connect_struct
{
  char gate[DEV_MAX_DAT_LEN];
  char pin[DEV_MAX_DAT_LEN];
  char pad[DEV_MAX_DAT_LEN];
};


struct device_struct
{
  char name[DEV_MAX_DAT_LEN];
  char package[DEV_MAX_DAT_LEN];
  int connect_cnt;
  struct connect_struct connects[DEV_MAX_CONNECTS];
  int technology_cnt;
  struct technology_struct technologies[DEV_MAX_TECHNOLOGIES];
};


struct gate_struct
{
  char name[DEV_MAX_DAT_LEN];
  char symbol[DEV_MAX_DAT_LEN];
};


struct deviceset_struct
{
  char name[DEV_MAX_DAT_LEN];
  char prefix[DEV_MAX_DAT_LEN];
  char description[DEV_MAX_DAT_LEN];
  int gate_cnt;
  struct gate_struct gates[DEV_MAX_GATES];
  int device_cnt;
  struct device_struct devices[DEV_MAX_DEVICES];
};


int get_deviceset_data(struct xml_handle *, struct deviceset_struct *);



#endif





