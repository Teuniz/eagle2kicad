/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2021 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/


#ifndef package_INCLUDED
#define package_INCLUDED

#include "global.h"
#include "utils.h"
#include "xml.h"



#define PACK_MAX_DAT_LEN          (256)

#define PACK_MAX_HOLES           (1024)

#define PACK_MAX_PADS            (1024)

#define PACK_MAX_SMDS            (1024)

#define PACK_MAX_TEXTS             (64)

#define PACK_MAX_WIRES           (1024)

#define PACK_MAX_CIRCLES         (1024)

#define PACK_MAX_RECTANGLES       (256)

#define PACK_MAX_POLYGONS         (256)

#define PACK_MAX_VERTICES         (256)

#define PACK_PAD_SHAPE_ROUND        (0)
#define PACK_PAD_SHAPE_SQUARE       (1)
#define PACK_PAD_SHAPE_OCTAGON      (2)
#define PACK_PAD_SHAPE_LONG         (3)
#define PACK_PAD_SHAPE_OFFSET       (4)

#define PACK_PIN_DIRECT_IO          (0)
#define PACK_PIN_DIRECT_NC          (1)
#define PACK_PIN_DIRECT_IN          (2)
#define PACK_PIN_DIRECT_OUT         (3)
#define PACK_PIN_DIRECT_OC          (4)
#define PACK_PIN_DIRECT_HIZ         (5)
#define PACK_PIN_DIRECT_PAS         (6)
#define PACK_PIN_DIRECT_PWR         (7)
#define PACK_PIN_DIRECT_SUP         (8)

#define PACK_PIN_FUNC_NONE          (0)
#define PACK_PIN_FUNC_DOT           (1)
#define PACK_PIN_FUNC_CLK           (2)
#define PACK_PIN_FUNC_DOTCLK        (3)

#define PACK_PIN_LEN_LONG           (0)
#define PACK_PIN_LEN_POINT          (1)
#define PACK_PIN_LEN_SHORT          (2)
#define PACK_PIN_LEN_MIDDLE         (3)

#define PACK_ROT_R0                 (0)
#define PACK_ROT_R90                (1)
#define PACK_ROT_R180               (2)
#define PACK_ROT_R270               (3)

#define PACK_PIN_VIS_BOTH           (0)
#define PACK_PIN_VIS_OFF            (1)
#define PACK_PIN_VIS_PAD            (2)
#define PACK_PIN_VIS_PIN            (3)

#define PACK_TXT_ALIGN_BOTTOMLEFT   (0 | 0)
#define PACK_TXT_ALIGN_BOTTOMCENTER (0 | 1)
#define PACK_TXT_ALIGN_BOTTOMRIGHT  (0 | 2)
#define PACK_TXT_ALIGN_CENTERLEFT   (4 | 0)
#define PACK_TXT_ALIGN_CENTER       (4 | 1)
#define PACK_TXT_ALIGN_CENTERRIGHT  (4 | 2)
#define PACK_TXT_ALIGN_TOPLEFT      (8 | 0)
#define PACK_TXT_ALIGN_TOPCENTER    (8 | 1)
#define PACK_TXT_ALIGN_TOPRIGHT     (8 | 2)

#define PACK_WIRE_STYLE_CONTINUOUS  (0)
#define PACK_WIRE_STYLE_LONGDASH    (1)
#define PACK_WIRE_STYLE_SHORTDASH   (2)
#define PACK_WIRE_STYLE_DASHDOT     (3)


struct pack_hole_struct
{
  double x;
  double y;
  double drill;
};


struct pack_pad_struct
{
  char name[PACK_MAX_DAT_LEN];
  double x;
  double y;
  double drill;
  double diameter;
  int shape;
  int rotation;
  int stop;
  int thermals;
  int first;
};


struct pack_smd_struct
{
  char name[PACK_MAX_DAT_LEN];
  double x;
  double y;
  double dx;
  double dy;
  int layer;
  int roundness;
  double rotation;
  int stop;
  int thermals;
  int cream;
};


struct pack_vertex_struct
{
  double x;
  double y;
  double curve;
};


struct pack_polygon_struct
{
  double width;
  int layer;
  int vertex_cnt;
  struct pack_vertex_struct vertices[PACK_MAX_VERTICES];
};


struct pack_rectangle_struct
{
  double x1;
  double y1;
  double x2;
  double y2;
  int layer;
  int rotation;
};


struct pack_circle_struct
{
  double x;
  double y;
  double radius;
  double width;
  int layer;
};


struct pack_wire_struct
{
  double x1;
  double y1;
  double x2;
  double y2;
  double width;
  double curve;
  int layer;
  int style;
};


struct pack_text_struct
{
  double x;
  double y;
  double size;
  int layer;
  int align;
  double rotation;
  char text[PACK_MAX_DAT_LEN];
};


struct package_struct
{
  char name[PACK_MAX_DAT_LEN];
  char description[PACK_MAX_DAT_LEN];
  int text_cnt;
  struct pack_text_struct texts[PACK_MAX_TEXTS];
  int wire_cnt;
  struct pack_wire_struct wires[PACK_MAX_WIRES];
  int circle_cnt;
  struct pack_circle_struct circles[PACK_MAX_CIRCLES];
  int rectangle_cnt;
  struct pack_rectangle_struct rectangles[PACK_MAX_RECTANGLES];
  int polygon_cnt;
  struct pack_polygon_struct polygons[PACK_MAX_POLYGONS];
  int hole_cnt;
  struct pack_hole_struct holes[PACK_MAX_HOLES];
  int pad_cnt;
  struct pack_pad_struct pads[PACK_MAX_PADS];
  int smd_cnt;
  struct pack_smd_struct smds[PACK_MAX_SMDS];
};


int get_package_data(struct xml_handle *, struct package_struct *, const char *);



#endif





