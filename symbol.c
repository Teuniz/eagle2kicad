/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2021 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/


#include "symbol.h"


static int get_pin_data(struct xml_handle *, struct symbol_struct *, const char *);
static int get_text_data(struct xml_handle *, struct symbol_struct *, const char *);
static int get_wire_data(struct xml_handle *, struct symbol_struct *, const char *);
static int get_circle_data(struct xml_handle *, struct symbol_struct *, const char *);
static int get_rectangle_data(struct xml_handle *, struct symbol_struct *, const char *);
static int get_polygon_data(struct xml_handle *, struct symbol_struct *, const char *);


int get_symbol_data(struct xml_handle *hdl, struct symbol_struct *sym, const char *req_name)
{
  int i,
      err;

  char str[MAX_STR_LEN]="";

  memset(sym, 0, sizeof(struct symbol_struct));

  xml_goto_root(hdl);

  if(xml_goto_nth_element_inside(hdl, "drawing", 0))
  {
    fprintf(stderr, "*** ERROR *** cannot find element \"drawing\" (-101)\n");
    return -101;
  }

  if(xml_goto_nth_element_inside(hdl, "library", 0))
  {
    fprintf(stderr, "*** ERROR *** cannot find element \"library\" (-102)\n");
    return -102;
  }

  if(xml_goto_nth_element_inside(hdl, "symbols", 0))
  {
    fprintf(stderr, "*** ERROR *** cannot find element \"devicesets\" (-103)\n");
    return -103;
  }

  for(i=0; ; i++)
  {
    if(xml_goto_nth_element_inside(hdl, "symbol", i))
    {
      return 1;  /* symbol not found */
    }

    if(xml_get_attribute_of_element(hdl, "name", str, MAX_STR_LEN) < 0)
    {
      fprintf(stderr, "*** ERROR *** XML error (-104)\n");
      return -104;
    }

    if(!strcmp(req_name, str))
    {
      break;
    }

    xml_go_up(hdl);
  }

  err = get_pin_data(hdl, sym, req_name);
  if(err)
  {
    return err;
  }

  err = get_text_data(hdl, sym, req_name);
  if(err)
  {
    return err;
  }

  err = get_wire_data(hdl, sym, req_name);
  if(err)
  {
    return err;
  }

  err = get_circle_data(hdl, sym, req_name);
  if(err)
  {
    return err;
  }

  err = get_rectangle_data(hdl, sym, req_name);
  if(err)
  {
    return err;
  }

  err = get_polygon_data(hdl, sym, req_name);
  if(err)
  {
    return err;
  }

  xml_go_up(hdl);

  return 0;
}


static int get_pin_data(struct xml_handle *hdl, struct symbol_struct *sym, const char *req_name)
{
  int i, j, len;

  char str[MAX_STR_LEN]="";

  for(i=0; i<SYM_MAX_PINS; i++)
  {
    if(xml_goto_nth_element_inside(hdl, "pin", i))
    {
      break;
    }

    if(xml_get_attribute_of_element(hdl, "name", str, MAX_STR_LEN) >= 0)
    {
      strlcpy(sym->pins[i].name, str, SYM_MAX_DAT_LEN);
    }

    if(xml_get_attribute_of_element(hdl, "x", str, MAX_STR_LEN) >= 0)
    {
      sym->pins[i].x = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "y", str, MAX_STR_LEN) >= 0)
    {
      sym->pins[i].y = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "direction", str, MAX_STR_LEN) >= 0)
    {
      if(!strcmp(str, "io"))
      {
        sym->pins[i].direction = SYM_PIN_DIRECT_IO;
      }
      else if(!strcmp(str, "nc"))
        {
          sym->pins[i].direction = SYM_PIN_DIRECT_NC;
        }
        else if(!strcmp(str, "in"))
          {
            sym->pins[i].direction = SYM_PIN_DIRECT_IN;
          }
          else if(!strcmp(str, "out"))
            {
              sym->pins[i].direction = SYM_PIN_DIRECT_OUT;
            }
            else if(!strcmp(str, "oc"))
              {
                sym->pins[i].direction = SYM_PIN_DIRECT_OC;
              }
              else if(!strcmp(str, "hiz"))
                {
                  sym->pins[i].direction = SYM_PIN_DIRECT_HIZ;
                }
                else if(!strcmp(str, "pas"))
                  {
                    sym->pins[i].direction = SYM_PIN_DIRECT_PAS;
                  }
                  else if(!strcmp(str, "pwr"))
                    {
                      sym->pins[i].direction = SYM_PIN_DIRECT_PWR;
                    }
                    else if(!strcmp(str, "sup"))
                      {
                        sym->pins[i].direction = SYM_PIN_DIRECT_SUP;
                      }
                      else
                      {
                        fprintf(stderr, "*** ERROR *** undefined pin direction in symbol: %s pin: %s direction: %s (-105)\n", req_name, sym->pins[i].name, str);
                        return -105;
                      }
    }

    if(xml_get_attribute_of_element(hdl, "function", str, MAX_STR_LEN) >= 0)
    {
      if(!strcmp(str, "none"))
      {
        sym->pins[i].function = SYM_PIN_FUNC_NONE;
      }
      else if(!strcmp(str, "dot"))
        {
          sym->pins[i].function = SYM_PIN_FUNC_DOT;
        }
        else if(!strcmp(str, "clk"))
          {
            sym->pins[i].function = SYM_PIN_FUNC_CLK;
          }
          else if(!strcmp(str, "dotclk"))
            {
              sym->pins[i].function = SYM_PIN_FUNC_DOTCLK;
            }
            else
            {
              fprintf(stderr, "*** ERROR *** undefined pin function in symbol: %s pin: %s direction: %s (-106)\n", req_name, sym->pins[i].name, str);
              return -106;
            }
    }

    if(xml_get_attribute_of_element(hdl, "length", str, MAX_STR_LEN) >= 0)
    {
      if(!strcmp(str, "long"))
      {
        sym->pins[i].length = SYM_PIN_LEN_LONG;
      }
      else if(!strcmp(str, "point"))
        {
          sym->pins[i].length = SYM_PIN_LEN_POINT;
        }
        else if(!strcmp(str, "short"))
          {
            sym->pins[i].length = SYM_PIN_LEN_SHORT;
          }
          else if(!strcmp(str, "middle"))
            {
              sym->pins[i].length = SYM_PIN_LEN_MIDDLE;
            }
            else
            {
              fprintf(stderr, "*** ERROR *** undefined pin length in symbol: %s pin: %s direction: %s (-107)\n", req_name, sym->pins[i].name, str);
              return -107;
            }
    }

    len = xml_get_attribute_of_element(hdl, "rot", str, MAX_STR_LEN);
    if(len >= 0)
    {
      for(j=0; j<len; j++)
      {
        if((str[j] >= '0') && (str[j] <= '9'))  break;
      }

      if(!strcmp(str + j, "0"))
      {
        sym->pins[i].rotation = SYM_ROT_R0;
      }
      else if(!strcmp(str + j, "90"))
        {
          sym->pins[i].rotation = SYM_ROT_R90;
        }
        else if(!strcmp(str + j, "180"))
          {
            sym->pins[i].rotation = SYM_ROT_R180;
          }
          else if(!strcmp(str + j, "270"))
            {
              sym->pins[i].rotation = SYM_ROT_R270;
            }
            else
            {
              fprintf(stderr, "*** ERROR *** undefined pin rotation in symbol: %s pin: %s rotation: %s (-108)\n", req_name, sym->pins[i].name, str);
              return -108;
            }
    }

    if(xml_get_attribute_of_element(hdl, "visible", str, MAX_STR_LEN) >= 0)
    {
      if(!strcmp(str, "both"))
      {
        sym->pins[i].visible = SYM_PIN_VIS_BOTH;
      }
      else if(!strcmp(str, "off"))
        {
          sym->pins[i].visible = SYM_PIN_VIS_OFF;
        }
        else if(!strcmp(str, "pad"))
          {
            sym->pins[i].visible = SYM_PIN_VIS_PAD;
          }
          else if(!strcmp(str, "pin"))
            {
              sym->pins[i].visible = SYM_PIN_VIS_PIN;
            }
            else
            {
              fprintf(stderr, "*** ERROR *** undefined pin visible in symbol: %s pin: %s visible: %s (-109)\n", req_name, sym->pins[i].name, str);
              return -109;
            }
    }

    if(xml_get_attribute_of_element(hdl, "swaplevel", str, MAX_STR_LEN) >= 0)
    {
      sym->pins[i].swaplevel = atoi(str);
    }

    sym->pin_cnt++;

    xml_go_up(hdl);
  }

  return 0;
}


static int get_text_data(struct xml_handle *hdl, struct symbol_struct *sym, const char *req_name)
{
  int i, j, len;

  char str[MAX_STR_LEN]="";

  for(i=0; i<SYM_MAX_TEXTS; i++)
  {
    if(xml_goto_nth_element_inside(hdl, "text", i))
    {
      break;
    }

    if(xml_get_attribute_of_element(hdl, "x", str, MAX_STR_LEN) >= 0)
    {
      sym->texts[i].x = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "y", str, MAX_STR_LEN) >= 0)
    {
      sym->texts[i].y = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "size", str, MAX_STR_LEN) >= 0)
    {
      sym->texts[i].size = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "layer", str, MAX_STR_LEN) >= 0)
    {
      sym->texts[i].layer = atoi(str);
    }

    if(xml_get_attribute_of_element(hdl, "align", str, MAX_STR_LEN) >= 0)
    {
      if(!strcmp(str, "bottom-left"))
      {
        sym->texts[i].align = SYM_TXT_ALIGN_BOTTOMLEFT;
      }
      else if(!strcmp(str, "bottom-center"))
        {
          sym->texts[i].align = SYM_TXT_ALIGN_BOTTOMCENTER;
        }
        else if(!strcmp(str, "bottom-right"))
          {
            sym->texts[i].align = SYM_TXT_ALIGN_BOTTOMRIGHT;
          }
          else if(!strcmp(str, "center-left"))
            {
              sym->texts[i].align = SYM_TXT_ALIGN_CENTERLEFT;
            }
            else if(!strcmp(str, "center"))
              {
                sym->texts[i].align = SYM_TXT_ALIGN_CENTER;
              }
              else if(!strcmp(str, "center-right"))
                {
                  sym->texts[i].align = SYM_TXT_ALIGN_CENTERRIGHT;
                }
                else if(!strcmp(str, "top-left"))
                  {
                    sym->texts[i].align = SYM_TXT_ALIGN_TOPLEFT;
                  }
                  else if(!strcmp(str, "top-center"))
                    {
                      sym->texts[i].align = SYM_TXT_ALIGN_TOPCENTER;
                    }
                    else if(!strcmp(str, "top-right"))
                      {
                        sym->texts[i].align = SYM_TXT_ALIGN_TOPRIGHT;
                      }
                      else
                      {
                        fprintf(stderr, "*** ERROR *** undefined text align in symbol: %s align: %s (-110)\n", req_name, str);
                        return -110;
                      }
    }

    len = xml_get_attribute_of_element(hdl, "rot", str, MAX_STR_LEN);
    if(len >= 0)
    {
      for(j=0; j<len; j++)
      {
        if((str[j] >= '0') && (str[j] <= '9'))  break;
      }

      if(!strcmp(str + j, "0"))
      {
        sym->texts[i].rotation = SYM_ROT_R0;
      }
      else if(!strcmp(str + j, "90"))
        {
          sym->texts[i].rotation = SYM_ROT_R90;
        }
        else if(!strcmp(str + j, "180"))
          {
            sym->texts[i].rotation = SYM_ROT_R180;
          }
          else if(!strcmp(str + j, "270"))
            {
              sym->texts[i].rotation = SYM_ROT_R270;
            }
            else
            {
              fprintf(stderr, "*** ERROR *** undefined text rotation in symbol: %s rotation: %s (-111)\n", req_name, str);
              return -111;
            }
    }

    xml_get_content_of_element(hdl, sym->texts[i].text, SYM_MAX_DAT_LEN);

    sym->text_cnt++;

    xml_go_up(hdl);
  }

  return 0;
}


static int get_wire_data(struct xml_handle *hdl, struct symbol_struct *sym, const char *req_name)
{
  int i;

  char str[MAX_STR_LEN]="";

  for(i=0; i<SYM_MAX_WIRES; i++)
  {
    if(xml_goto_nth_element_inside(hdl, "wire", i))
    {
      break;
    }

    if(xml_get_attribute_of_element(hdl, "x1", str, MAX_STR_LEN) >= 0)
    {
      sym->wires[i].x1 = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "y1", str, MAX_STR_LEN) >= 0)
    {
      sym->wires[i].y1 = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "x2", str, MAX_STR_LEN) >= 0)
    {
      sym->wires[i].x2 = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "y2", str, MAX_STR_LEN) >= 0)
    {
      sym->wires[i].y2 = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "width", str, MAX_STR_LEN) >= 0)
    {
      sym->wires[i].width = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "curve", str, MAX_STR_LEN) >= 0)
    {
      sym->wires[i].curve = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "layer", str, MAX_STR_LEN) >= 0)
    {
      sym->wires[i].layer = atoi(str);
    }

    if(xml_get_attribute_of_element(hdl, "style", str, MAX_STR_LEN) >= 0)
    {
      if(!strcmp(str, "continuous"))
      {
        sym->wires[i].style = SYM_WIRE_STYLE_CONTINUOUS;
      }
      else if(!strcmp(str, "longdash"))
        {
          sym->wires[i].style = SYM_WIRE_STYLE_LONGDASH;
        }
        else if(!strcmp(str, "shortdash"))
          {
            sym->wires[i].style = SYM_WIRE_STYLE_SHORTDASH;
          }
          else if(!strcmp(str, "dashdot"))
            {
              sym->wires[i].style = SYM_WIRE_STYLE_DASHDOT;
            }
            else
            {
              fprintf(stderr, "*** ERROR *** undefined wire style in symbol: %s style: %s (-120)\n", req_name, str);
              return -120;
            }
    }

    sym->wire_cnt++;

    xml_go_up(hdl);
  }

  return 0;
}


static int get_circle_data(struct xml_handle *hdl, struct symbol_struct *sym, __attribute__((unused)) const char *req_name)
{
  int i;

  char str[MAX_STR_LEN]="";

  for(i=0; i<SYM_MAX_CIRCLES; i++)
  {
    if(xml_goto_nth_element_inside(hdl, "circle", i))
    {
      break;
    }

    if(xml_get_attribute_of_element(hdl, "x", str, MAX_STR_LEN) >= 0)
    {
      sym->circles[i].x = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "y", str, MAX_STR_LEN) >= 0)
    {
      sym->circles[i].y = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "radius", str, MAX_STR_LEN) >= 0)
    {
      sym->circles[i].radius = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "width", str, MAX_STR_LEN) >= 0)
    {
      sym->circles[i].width = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "layer", str, MAX_STR_LEN) >= 0)
    {
      sym->circles[i].layer = atoi(str);
    }

    sym->circle_cnt++;

    xml_go_up(hdl);
  }

  return 0;
}


static int get_rectangle_data(struct xml_handle *hdl, struct symbol_struct *sym, const char *req_name)
{
  int i, j, len;

  char str[MAX_STR_LEN]="";

  for(i=0; i<SYM_MAX_RECTANGLES; i++)
  {
    if(xml_goto_nth_element_inside(hdl, "rectangle", i))
    {
      break;
    }

    if(xml_get_attribute_of_element(hdl, "x1", str, MAX_STR_LEN) >= 0)
    {
      sym->rectangles[i].x1 = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "y1", str, MAX_STR_LEN) >= 0)
    {
      sym->rectangles[i].y1 = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "x2", str, MAX_STR_LEN) >= 0)
    {
      sym->rectangles[i].x2 = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "y2", str, MAX_STR_LEN) >= 0)
    {
      sym->rectangles[i].y2 = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "layer", str, MAX_STR_LEN) >= 0)
    {
      sym->rectangles[i].layer = atoi(str);
    }

    len = xml_get_attribute_of_element(hdl, "rot", str, MAX_STR_LEN);
    if(len >= 0)
    {
      for(j=0; j<len; j++)
      {
        if((str[j] >= '0') && (str[j] <= '9'))  break;
      }

      if(!strcmp(str + j, "0"))
      {
        sym->rectangles[i].rotation = SYM_ROT_R0;
      }
      else if(!strcmp(str + j, "90"))
        {
          sym->rectangles[i].rotation = SYM_ROT_R90;
        }
        else if(!strcmp(str + j, "180"))
          {
            sym->rectangles[i].rotation = SYM_ROT_R180;
          }
          else if(!strcmp(str + j, "270"))
            {
              sym->rectangles[i].rotation = SYM_ROT_R270;
            }
            else
            {
              fprintf(stderr, "*** ERROR *** undefined rectangle rotation in symbol: %s rotation: %s (-130)\n", req_name, str);
              return -130;
            }
    }

    sym->rectangle_cnt++;

    xml_go_up(hdl);
  }

  return 0;
}


static int get_polygon_data(struct xml_handle *hdl, struct symbol_struct *sym, __attribute__((unused)) const char *req_name)
{
  int i, j;

  char str[MAX_STR_LEN]="";

  for(i=0; i<SYM_MAX_POLYGONS; i++)
  {
    if(xml_goto_nth_element_inside(hdl, "polygon", i))
    {
      break;
    }

    if(xml_get_attribute_of_element(hdl, "width", str, MAX_STR_LEN) >= 0)
    {
      sym->polygons[i].width = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "layer", str, MAX_STR_LEN) >= 0)
    {
      sym->polygons[i].layer = atoi(str);
    }

    for(j=0; j<SYM_MAX_VERTICES; j++)
    {
      if(xml_goto_nth_element_inside(hdl, "vertex", j))
      {
        break;
      }

      if(xml_get_attribute_of_element(hdl, "x", str, MAX_STR_LEN) >= 0)
      {
        sym->polygons[i].vertices[j].x = atof(str);
      }

      if(xml_get_attribute_of_element(hdl, "y", str, MAX_STR_LEN) >= 0)
      {
        sym->polygons[i].vertices[j].y = atof(str);
      }

      if(xml_get_attribute_of_element(hdl, "curve", str, MAX_STR_LEN) >= 0)
      {
        sym->polygons[i].vertices[j].curve = atof(str);
      }

      sym->polygons[i].vertex_cnt++;

      xml_go_up(hdl);
    }

    sym->polygon_cnt++;

    xml_go_up(hdl);
  }

  return 0;
}










