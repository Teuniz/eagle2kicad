/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2021 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/


#ifndef E2K_GLOBAL_H
#define E2K_GLOBAL_H

#define MAX_STR_LEN              (4096)

#define MAX_PATH_LEN             (1024)

#define MAX_PACKAGES             (2048)

#define MAX_DEVICESETS_LIST      (2048)

#define MAX_LIBNAME_LEN           (128)

#define E_SCH_NETS_LAYER          (91)
#define E_SCH_BUSSES_LAYER        (92)
#define E_SCH_PINS_LAYER          (93)
#define E_SCH_SYMBOLS_LAYER       (94)
#define E_SCH_NAMES_LAYER         (95)
#define E_SCH_VALUES_LAYER        (96)
#define E_SCH_INFO_LAYER          (97)
#define E_SCH_GUIDE_LAYER         (98)

#define E_PCB_TOP_LAYER            (1)
#define E_PCB_ROUTE2_LAYER         (2)
#define E_PCB_ROUTE3_LAYER         (3)
#define E_PCB_ROUTE4_LAYER         (4)
#define E_PCB_ROUTE5_LAYER         (5)
#define E_PCB_ROUTE6_LAYER         (6)
#define E_PCB_ROUTE7_LAYER         (7)
#define E_PCB_ROUTE8_LAYER         (8)
#define E_PCB_ROUTE9_LAYER         (9)
#define E_PCB_ROUTE10_LAYER       (10)
#define E_PCB_ROUTE11_LAYER       (11)
#define E_PCB_ROUTE12_LAYER       (12)
#define E_PCB_ROUTE13_LAYER       (13)
#define E_PCB_ROUTE14_LAYER       (14)
#define E_PCB_ROUTE15_LAYER       (15)
#define E_PCB_BOTTOM_LAYER        (16)
#define E_PCB_PADS_LAYER          (17)
#define E_PCB_VIAS_LAYER          (18)

#define E_PCB_DIMENSION_LAYER     (20)
#define E_PCB_TPLACE_LAYER        (21)
#define E_PCB_BPLACE_LAYER        (22)
#define E_PCB_TORIGINS_LAYER      (23)
#define E_PCB_BORIGINS_LAYER      (24)
#define E_PCB_TNAMES_LAYER        (25)
#define E_PCB_BNAMES_LAYER        (26)
#define E_PCB_TVALUES_LAYER       (27)
#define E_PCB_BVALUES_LAYER       (28)
#define E_PCB_TSTOP_LAYER         (29)
#define E_PCB_BSTOP_LAYER         (30)
#define E_PCB_TCREAM_LAYER        (31)
#define E_PCB_BCREAM_LAYER        (32)
#define E_PCB_TFINISH_LAYER       (33)
#define E_PCB_BFINISH_LAYER       (34)
#define E_PCB_TGLUE_LAYER         (35)
#define E_PCB_BGLUE_LAYER         (36)
#define E_PCB_TTEST_LAYER         (37)
#define E_PCB_BTEST_LAYER         (38)
#define E_PCB_TKEEPOUT_LAYER      (39)
#define E_PCB_BKEEPOUT_LAYER      (40)
#define E_PCB_TRESTRICT_LAYER     (41)
#define E_PCB_BRESTRICT_LAYER     (42)
#define E_PCB_VRESTRICT_LAYER     (43)
#define E_PCB_DRILLS_LAYER        (44)
#define E_PCB_HOLES_LAYER         (45)
#define E_PCB_MILLING_LAYER       (46)
#define E_PCB_MEASURES_LAYER      (47)
#define E_PCB_DOCUMENT_LAYER      (48)
#define E_PCB_REFERENCE_LAYER     (49)
#define E_PCB_TDOCU_LAYER         (51)
#define E_PCB_BDOCU_LAYER         (52)

#endif



















