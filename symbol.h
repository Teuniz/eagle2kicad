/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2021 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/


#ifndef symbol_INCLUDED
#define symbol_INCLUDED

#include "global.h"
#include "utils.h"
#include "xml.h"



#define SYM_MAX_DAT_LEN          (256)

#define SYM_MAX_PINS            (1024)

#define SYM_MAX_TEXTS             (64)

#define SYM_MAX_WIRES           (1024)

#define SYM_MAX_CIRCLES         (1024)

#define SYM_MAX_RECTANGLES       (256)

#define SYM_MAX_POLYGONS         (256)

#define SYM_MAX_VERTICES         (256)

#define SYM_PIN_DIRECT_IO          (0)
#define SYM_PIN_DIRECT_NC          (1)
#define SYM_PIN_DIRECT_IN          (2)
#define SYM_PIN_DIRECT_OUT         (3)
#define SYM_PIN_DIRECT_OC          (4)
#define SYM_PIN_DIRECT_HIZ         (5)
#define SYM_PIN_DIRECT_PAS         (6)
#define SYM_PIN_DIRECT_PWR         (7)
#define SYM_PIN_DIRECT_SUP         (8)

#define SYM_PIN_FUNC_NONE          (0)
#define SYM_PIN_FUNC_DOT           (1)
#define SYM_PIN_FUNC_CLK           (2)
#define SYM_PIN_FUNC_DOTCLK        (3)

#define SYM_PIN_LEN_LONG           (0)
#define SYM_PIN_LEN_POINT          (1)
#define SYM_PIN_LEN_SHORT          (2)
#define SYM_PIN_LEN_MIDDLE         (3)

#define SYM_ROT_R0                 (0)
#define SYM_ROT_R90                (1)
#define SYM_ROT_R180               (2)
#define SYM_ROT_R270               (3)

#define SYM_PIN_VIS_BOTH           (0)
#define SYM_PIN_VIS_OFF            (1)
#define SYM_PIN_VIS_PAD            (2)
#define SYM_PIN_VIS_PIN            (3)

#define SYM_TXT_ALIGN_BOTTOMLEFT   (0 | 0)
#define SYM_TXT_ALIGN_BOTTOMCENTER (0 | 1)
#define SYM_TXT_ALIGN_BOTTOMRIGHT  (0 | 2)
#define SYM_TXT_ALIGN_CENTERLEFT   (4 | 0)
#define SYM_TXT_ALIGN_CENTER       (4 | 1)
#define SYM_TXT_ALIGN_CENTERRIGHT  (4 | 2)
#define SYM_TXT_ALIGN_TOPLEFT      (8 | 0)
#define SYM_TXT_ALIGN_TOPCENTER    (8 | 1)
#define SYM_TXT_ALIGN_TOPRIGHT     (8 | 2)

#define SYM_WIRE_STYLE_CONTINUOUS  (0)
#define SYM_WIRE_STYLE_LONGDASH    (1)
#define SYM_WIRE_STYLE_SHORTDASH   (2)
#define SYM_WIRE_STYLE_DASHDOT     (3)


struct sym_vertex_struct
{
  double x;
  double y;
  double curve;
};


struct sym_polygon_struct
{
  double width;
  int layer;
  int vertex_cnt;
  struct sym_vertex_struct vertices[SYM_MAX_VERTICES];
};


struct sym_rectangle_struct
{
  double x1;
  double y1;
  double x2;
  double y2;
  int layer;
  int rotation;
};


struct sym_circle_struct
{
  double x;
  double y;
  double radius;
  double width;
  int layer;
};


struct sym_wire_struct
{
  double x1;
  double y1;
  double x2;
  double y2;
  double width;
  double curve;
  int layer;
  int style;
};


struct sym_text_struct
{
  double x;
  double y;
  double size;
  int layer;
  int align;
  int rotation;
  char text[SYM_MAX_DAT_LEN];
};


struct pin_struct
{
  char name[SYM_MAX_DAT_LEN];
  double x;
  double y;
  int direction;
  int function;
  int length;
  int rotation;
  int visible;
  int swaplevel;
};


struct symbol_struct
{
  char name[SYM_MAX_DAT_LEN];
  int pin_cnt;
  struct pin_struct pins[SYM_MAX_PINS];
  int text_cnt;
  struct sym_text_struct texts[SYM_MAX_TEXTS];
  int wire_cnt;
  struct sym_wire_struct wires[SYM_MAX_WIRES];
  int circle_cnt;
  struct sym_circle_struct circles[SYM_MAX_CIRCLES];
  int rectangle_cnt;
  struct sym_rectangle_struct rectangles[SYM_MAX_RECTANGLES];
  int polygon_cnt;
  struct sym_polygon_struct polygons[SYM_MAX_POLYGONS];
};


int get_symbol_data(struct xml_handle *, struct symbol_struct *, const char *);



#endif





