#
#
# author: Teunis van Beelen
#
# email: teuniz@protonmail.com
#
#

CC = gcc
CFLAGS = -std=gnu11 -O2 -Wall -Wextra -Wshadow -Wformat-nonliteral -Wformat-security -Wtype-limits -Wfatal-errors
LDLIBS = -lm

objects = obj/main.o obj/utils.o obj/xml.o obj/deviceset.o obj/symbol.o obj/package.o

all: eagle2kicad

eagle2kicad:	$(objects)
	$(CC) $(objects) -o eagle2kicad $(LDLIBS)

obj/main.o:		main.c global.h utils.h xml.h deviceset.h symbol.h package.h
	$(CC) $(CFLAGS) -c main.c -o obj/main.o

obj/utils.o:	utils.h utils.c
	$(CC) $(CFLAGS) -c utils.c -o obj/utils.o

obj/xml.o:	xml.h xml.c
	$(CC) $(CFLAGS) -c xml.c -o obj/xml.o

obj/deviceset.o:	deviceset.h deviceset.c utils.h xml.h global.h
	$(CC) $(CFLAGS) -c deviceset.c -o obj/deviceset.o

obj/symbol.o:	symbol.h symbol.c utils.h xml.h global.h
	$(CC) $(CFLAGS) -c symbol.c -o obj/symbol.o

obj/package.o:	package.h package.c utils.h xml.h global.h
	$(CC) $(CFLAGS) -c package.c -o obj/package.o

clean:
	$(RM) eagle2kicad $(objects)
