/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2021 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/


#include "package.h"


static int get_text_data(struct xml_handle *, struct package_struct *, const char *);
static int get_wire_data(struct xml_handle *, struct package_struct *, const char *);
static int get_circle_data(struct xml_handle *, struct package_struct *, const char *);
static int get_rectangle_data(struct xml_handle *, struct package_struct *, const char *);
static int get_polygon_data(struct xml_handle *, struct package_struct *, const char *);
static int get_hole_data(struct xml_handle *, struct package_struct *, const char *);
static int get_pad_data(struct xml_handle *, struct package_struct *, const char *);
static int get_smd_data(struct xml_handle *, struct package_struct *, const char *);


int get_package_data(struct xml_handle *hdl, struct package_struct *pack, const char *req_name)
{
  int i,
      err;

  char str[MAX_STR_LEN]="";

  memset(pack, 0, sizeof(struct package_struct));

  xml_goto_root(hdl);

  if(xml_goto_nth_element_inside(hdl, "drawing", 0))
  {
    fprintf(stderr, "*** ERROR *** cannot find element \"drawing\" (-501)\n");
    return -501;
  }

  if(xml_goto_nth_element_inside(hdl, "library", 0))
  {
    fprintf(stderr, "*** ERROR *** cannot find element \"library\" (-502)\n");
    return -502;
  }

  if(xml_goto_nth_element_inside(hdl, "packages", 0))
  {
    fprintf(stderr, "*** ERROR *** cannot find element \"devicesets\" (-503)\n");
    return -503;
  }

  for(i=0; ; i++)
  {
    if(xml_goto_nth_element_inside(hdl, "package", i))
    {
      return 1;  /* package not found */
    }

    if(xml_get_attribute_of_element(hdl, "name", str, MAX_STR_LEN) < 0)
    {
      fprintf(stderr, "*** ERROR *** XML error (-504)\n");
      return -504;
    }

    if(!strcmp(req_name, str))
    {
      strlcpy(pack->name, str, PACK_MAX_DAT_LEN);

      if(!xml_goto_nth_element_inside(hdl, "description", 0))
      {
        xml_get_content_of_element(hdl, pack->description, PACK_MAX_DAT_LEN);

        xml_go_up(hdl);
      }

      break;
    }

    xml_go_up(hdl);
  }

  err = get_text_data(hdl, pack, req_name);
  if(err)
  {
    return err;
  }

  err = get_wire_data(hdl, pack, req_name);
  if(err)
  {
    return err;
  }

  err = get_circle_data(hdl, pack, req_name);
  if(err)
  {
    return err;
  }

  err = get_rectangle_data(hdl, pack, req_name);
  if(err)
  {
    return err;
  }

  err = get_polygon_data(hdl, pack, req_name);
  if(err)
  {
    return err;
  }

  err = get_hole_data(hdl, pack, req_name);
  if(err)
  {
    return err;
  }

  err = get_pad_data(hdl, pack, req_name);
  if(err)
  {
    return err;
  }

  err = get_smd_data(hdl, pack, req_name);
  if(err)
  {
    return err;
  }

  xml_go_up(hdl);

  return 0;
}


static int get_text_data(struct xml_handle *hdl, struct package_struct *pack, const char *req_name)
{
  int i, j, len;

  char str[MAX_STR_LEN]="";

  for(i=0; i<PACK_MAX_TEXTS; i++)
  {
    if(xml_goto_nth_element_inside(hdl, "text", i))
    {
      break;
    }

    if(xml_get_attribute_of_element(hdl, "x", str, MAX_STR_LEN) >= 0)
    {
      pack->texts[i].x = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "y", str, MAX_STR_LEN) >= 0)
    {
      pack->texts[i].y = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "size", str, MAX_STR_LEN) >= 0)
    {
      pack->texts[i].size = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "layer", str, MAX_STR_LEN) >= 0)
    {
      pack->texts[i].layer = atoi(str);
    }

    if(xml_get_attribute_of_element(hdl, "align", str, MAX_STR_LEN) >= 0)
    {
      if(!strcmp(str, "bottom-left"))
      {
        pack->texts[i].align = PACK_TXT_ALIGN_BOTTOMLEFT;
      }
      else if(!strcmp(str, "bottom-center"))
        {
          pack->texts[i].align = PACK_TXT_ALIGN_BOTTOMCENTER;
        }
        else if(!strcmp(str, "bottom-right"))
          {
            pack->texts[i].align = PACK_TXT_ALIGN_BOTTOMRIGHT;
          }
          else if(!strcmp(str, "center-left"))
            {
              pack->texts[i].align = PACK_TXT_ALIGN_CENTERLEFT;
            }
            else if(!strcmp(str, "center"))
              {
                pack->texts[i].align = PACK_TXT_ALIGN_CENTER;
              }
              else if(!strcmp(str, "center-right"))
                {
                  pack->texts[i].align = PACK_TXT_ALIGN_CENTERRIGHT;
                }
                else if(!strcmp(str, "top-left"))
                  {
                    pack->texts[i].align = PACK_TXT_ALIGN_TOPLEFT;
                  }
                  else if(!strcmp(str, "top-center"))
                    {
                      pack->texts[i].align = PACK_TXT_ALIGN_TOPCENTER;
                    }
                    else if(!strcmp(str, "top-right"))
                      {
                        pack->texts[i].align = PACK_TXT_ALIGN_TOPRIGHT;
                      }
                      else
                      {
                        fprintf(stderr, "*** ERROR *** undefined text align in package: %s align: %s (-510)\n", req_name, str);
                        return -510;
                      }
    }

    len = xml_get_attribute_of_element(hdl, "rot", str, MAX_STR_LEN);
    if(len >= 0)
    {
      for(j=0; j<len; j++)
      {
        if((str[j] >= '0') && (str[j] <= '9'))  break;
      }

      pack->texts[i].rotation = atof(str + j);

      if((pack->texts[i].rotation >= 359.900001) || (pack->texts[i].rotation <= -0.000001))
      {
        fprintf(stderr, "*** ERROR *** invalid value for text rotation in package: %s rotation: %s (-511)\n", req_name, str);
        return -511;
      }
    }

    xml_get_content_of_element(hdl, pack->texts[i].text, PACK_MAX_DAT_LEN);

    pack->text_cnt++;

    xml_go_up(hdl);
  }

  return 0;
}


static int get_wire_data(struct xml_handle *hdl, struct package_struct *pack, const char *req_name)
{
  int i;

  char str[MAX_STR_LEN]="";

  for(i=0; i<PACK_MAX_WIRES; i++)
  {
    if(xml_goto_nth_element_inside(hdl, "wire", i))
    {
      break;
    }

    if(xml_get_attribute_of_element(hdl, "x1", str, MAX_STR_LEN) >= 0)
    {
      pack->wires[i].x1 = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "y1", str, MAX_STR_LEN) >= 0)
    {
      pack->wires[i].y1 = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "x2", str, MAX_STR_LEN) >= 0)
    {
      pack->wires[i].x2 = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "y2", str, MAX_STR_LEN) >= 0)
    {
      pack->wires[i].y2 = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "width", str, MAX_STR_LEN) >= 0)
    {
      pack->wires[i].width = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "curve", str, MAX_STR_LEN) >= 0)
    {
      pack->wires[i].curve = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "layer", str, MAX_STR_LEN) >= 0)
    {
      pack->wires[i].layer = atoi(str);
    }

    if(xml_get_attribute_of_element(hdl, "style", str, MAX_STR_LEN) >= 0)
    {
      if(!strcmp(str, "continuous"))
      {
        pack->wires[i].style = PACK_WIRE_STYLE_CONTINUOUS;
      }
      else if(!strcmp(str, "longdash"))
        {
          pack->wires[i].style = PACK_WIRE_STYLE_LONGDASH;
        }
        else if(!strcmp(str, "shortdash"))
          {
            pack->wires[i].style = PACK_WIRE_STYLE_SHORTDASH;
          }
          else if(!strcmp(str, "dashdot"))
            {
              pack->wires[i].style = PACK_WIRE_STYLE_DASHDOT;
            }
            else
            {
              fprintf(stderr, "*** ERROR *** undefined wire style in package: %s style: %s (-520)\n", req_name, str);
              return -520;
            }
    }

    pack->wire_cnt++;

    xml_go_up(hdl);
  }

  return 0;
}


static int get_circle_data(struct xml_handle *hdl, struct package_struct *pack, __attribute__((unused)) const char *req_name)
{
  int i;

  char str[MAX_STR_LEN]="";

  for(i=0; i<PACK_MAX_CIRCLES; i++)
  {
    if(xml_goto_nth_element_inside(hdl, "circle", i))
    {
      break;
    }

    if(xml_get_attribute_of_element(hdl, "x", str, MAX_STR_LEN) >= 0)
    {
      pack->circles[i].x = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "y", str, MAX_STR_LEN) >= 0)
    {
      pack->circles[i].y = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "radius", str, MAX_STR_LEN) >= 0)
    {
      pack->circles[i].radius = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "width", str, MAX_STR_LEN) >= 0)
    {
      pack->circles[i].width = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "layer", str, MAX_STR_LEN) >= 0)
    {
      pack->circles[i].layer = atoi(str);
    }

    pack->circle_cnt++;

    xml_go_up(hdl);
  }

  return 0;
}


static int get_rectangle_data(struct xml_handle *hdl, struct package_struct *pack, const char *req_name)
{
  int i, j, len;

  char str[MAX_STR_LEN]="";

  for(i=0; i<PACK_MAX_RECTANGLES; i++)
  {
    if(xml_goto_nth_element_inside(hdl, "rectangle", i))
    {
      break;
    }

    if(xml_get_attribute_of_element(hdl, "x1", str, MAX_STR_LEN) >= 0)
    {
      pack->rectangles[i].x1 = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "y1", str, MAX_STR_LEN) >= 0)
    {
      pack->rectangles[i].y1 = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "x2", str, MAX_STR_LEN) >= 0)
    {
      pack->rectangles[i].x2 = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "y2", str, MAX_STR_LEN) >= 0)
    {
      pack->rectangles[i].y2 = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "layer", str, MAX_STR_LEN) >= 0)
    {
      pack->rectangles[i].layer = atoi(str);
    }

    len = xml_get_attribute_of_element(hdl, "rot", str, MAX_STR_LEN);
    if(len >= 0)
    {
      for(j=0; j<len; j++)
      {
        if((str[j] >= '0') && (str[j] <= '9'))  break;
      }

      if(!strcmp(str + j, "0"))
      {
        pack->rectangles[i].rotation = PACK_ROT_R0;
      }
      else if(!strcmp(str + j, "90"))
        {
          pack->rectangles[i].rotation = PACK_ROT_R90;
        }
        else if(!strcmp(str + j, "180"))
          {
            pack->rectangles[i].rotation = PACK_ROT_R180;
          }
          else if(!strcmp(str + j, "270"))
            {
              pack->rectangles[i].rotation = PACK_ROT_R270;
            }
            else
            {
              fprintf(stderr, "*** ERROR *** undefined rectangle rotation in package: %s rotation: %s (-530)\n", req_name, str);
              return -530;
            }
    }

    pack->rectangle_cnt++;

    xml_go_up(hdl);
  }

  return 0;
}


static int get_polygon_data(struct xml_handle *hdl, struct package_struct *pack, __attribute__((unused)) const char *req_name)
{
  int i, j;

  char str[MAX_STR_LEN]="";

  for(i=0; i<PACK_MAX_POLYGONS; i++)
  {
    if(xml_goto_nth_element_inside(hdl, "polygon", i))
    {
      break;
    }

    if(xml_get_attribute_of_element(hdl, "width", str, MAX_STR_LEN) >= 0)
    {
      pack->polygons[i].width = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "layer", str, MAX_STR_LEN) >= 0)
    {
      pack->polygons[i].layer = atoi(str);
    }

    for(j=0; j<PACK_MAX_VERTICES; j++)
    {
      if(xml_goto_nth_element_inside(hdl, "vertex", j))
      {
        break;
      }

      if(xml_get_attribute_of_element(hdl, "x", str, MAX_STR_LEN) >= 0)
      {
        pack->polygons[i].vertices[j].x = atof(str);
      }

      if(xml_get_attribute_of_element(hdl, "y", str, MAX_STR_LEN) >= 0)
      {
        pack->polygons[i].vertices[j].y = atof(str);
      }

      if(xml_get_attribute_of_element(hdl, "curve", str, MAX_STR_LEN) >= 0)
      {
        pack->polygons[i].vertices[j].curve = atof(str);
      }

      pack->polygons[i].vertex_cnt++;

      xml_go_up(hdl);
    }

    pack->polygon_cnt++;

    xml_go_up(hdl);
  }

  return 0;
}


int get_hole_data(struct xml_handle *hdl, struct package_struct *pack, __attribute__((unused)) const char *req_name)
{
  int i;

  char str[MAX_STR_LEN]="";

  for(i=0; i<PACK_MAX_HOLES; i++)
  {
    if(xml_goto_nth_element_inside(hdl, "hole", i))
    {
      break;
    }

    if(xml_get_attribute_of_element(hdl, "x", str, MAX_STR_LEN) >= 0)
    {
      pack->holes[i].x = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "y", str, MAX_STR_LEN) >= 0)
    {
      pack->holes[i].y = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "drill", str, MAX_STR_LEN) >= 0)
    {
      pack->holes[i].drill = atof(str);
    }

    pack->hole_cnt++;

    xml_go_up(hdl);
  }

  return 0;
}


int get_pad_data(struct xml_handle *hdl, struct package_struct *pack, const char *req_name)
{
  int i, j, len;

  char str[MAX_STR_LEN]="";

  for(i=0; i<PACK_MAX_PADS; i++)
  {
    if(xml_goto_nth_element_inside(hdl, "pad", i))
    {
      break;
    }

    if(xml_get_attribute_of_element(hdl, "name", str, MAX_STR_LEN) >= 0)
    {
      strlcpy(pack->pads[i].name, str, PACK_MAX_DAT_LEN);
    }

    if(xml_get_attribute_of_element(hdl, "x", str, MAX_STR_LEN) >= 0)
    {
      pack->pads[i].x = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "y", str, MAX_STR_LEN) >= 0)
    {
      pack->pads[i].y = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "drill", str, MAX_STR_LEN) >= 0)
    {
      pack->pads[i].drill = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "diameter", str, MAX_STR_LEN) >= 0)
    {
      pack->pads[i].diameter = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "shape", str, MAX_STR_LEN) >= 0)
    {
      if(!strcmp(str, "square"))
      {
        pack->pads[i].shape = PACK_PAD_SHAPE_SQUARE;
      }
      else if(!strcmp(str, "round"))
        {
          pack->pads[i].shape = PACK_PAD_SHAPE_ROUND;
        }
        else if(!strcmp(str, "octagon"))
          {
            pack->pads[i].shape = PACK_PAD_SHAPE_OCTAGON;
          }
          else if(!strcmp(str, "long"))
            {
              pack->pads[i].shape = PACK_PAD_SHAPE_LONG;
            }
            else if(!strcmp(str, "offset"))
              {
                pack->pads[i].shape = PACK_PAD_SHAPE_OFFSET;
              }
              else
              {
                fprintf(stderr, "*** ERROR *** undefined pad shape in package: %s shape: %s (-540)\n", req_name, str);
                return -540;
              }
    }

    len = xml_get_attribute_of_element(hdl, "rot", str, MAX_STR_LEN);
    if(len >= 0)
    {
      for(j=0; j<len; j++)
      {
        if((str[j] >= '0') && (str[j] <= '9'))  break;
      }

      if(!strcmp(str + j, "0"))
      {
        pack->pads[i].rotation = PACK_ROT_R0;
      }
      else if(!strcmp(str + j, "90"))
        {
          pack->pads[i].rotation = PACK_ROT_R90;
        }
        else if(!strcmp(str + j, "180"))
          {
            pack->pads[i].rotation = PACK_ROT_R180;
          }
          else if(!strcmp(str + j, "270"))
            {
              pack->pads[i].rotation = PACK_ROT_R270;
            }
            else
            {
              fprintf(stderr, "*** ERROR *** undefined pad rotation in package: %s rotation: %s (-541)\n", req_name, str);
              return -541;
            }
    }

    if(xml_get_attribute_of_element(hdl, "stop", str, MAX_STR_LEN) >= 0)
    {
      if(!strcmp(str, "yes"))
      {
        pack->pads[i].stop = 1;
      }
      else if(!strcmp(str, "no"))
        {
          pack->pads[i].stop = 0;
        }
        else
        {
          fprintf(stderr, "*** ERROR *** undefined pad stop in package: %s stop: %s (-542)\n", req_name, str);
          return -542;
        }
    }
    else
    {
      pack->pads[i].stop = 1;
    }

    if(xml_get_attribute_of_element(hdl, "thermals", str, MAX_STR_LEN) >= 0)
    {
      if(!strcmp(str, "yes"))
      {
        pack->pads[i].thermals = 1;
      }
      else if(!strcmp(str, "no"))
        {
          pack->pads[i].thermals = 0;
        }
        else
        {
          fprintf(stderr, "*** ERROR *** undefined pad thermals in package: %s thermals: %s (-543)\n", req_name, str);
          return -543;
        }
    }
    else
    {
      pack->pads[i].thermals = 1;
    }

    if(xml_get_attribute_of_element(hdl, "first", str, MAX_STR_LEN) >= 0)
    {
      if(!strcmp(str, "yes"))
      {
        pack->pads[i].first = 1;
      }
      else if(!strcmp(str, "no"))
        {
          pack->pads[i].first = 0;
        }
        else
        {
          fprintf(stderr, "*** ERROR *** undefined pad first in package: %s first: %s (-544)\n", req_name, str);
          return -544;
        }
    }
    else
    {
      pack->pads[i].first = 0;
    }

    pack->pad_cnt++;

    xml_go_up(hdl);
  }

  return 0;
}


int get_smd_data(struct xml_handle *hdl, struct package_struct *pack, const char *req_name)
{
  int i, j, len;

  char str[MAX_STR_LEN]="";

  for(i=0; i<PACK_MAX_SMDS; i++)
  {
    if(xml_goto_nth_element_inside(hdl, "smd", i))
    {
      break;
    }

    if(xml_get_attribute_of_element(hdl, "name", str, MAX_STR_LEN) >= 0)
    {
      strlcpy(pack->smds[i].name, str, PACK_MAX_DAT_LEN);
    }

    if(xml_get_attribute_of_element(hdl, "x", str, MAX_STR_LEN) >= 0)
    {
      pack->smds[i].x = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "y", str, MAX_STR_LEN) >= 0)
    {
      pack->smds[i].y = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "dx", str, MAX_STR_LEN) >= 0)
    {
      pack->smds[i].dx = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "dy", str, MAX_STR_LEN) >= 0)
    {
      pack->smds[i].dy = atof(str);
    }

    if(xml_get_attribute_of_element(hdl, "layer", str, MAX_STR_LEN) >= 0)
    {
      pack->smds[i].layer = atoi(str);
    }

    if(xml_get_attribute_of_element(hdl, "roundness", str, MAX_STR_LEN) >= 0)
    {
      pack->smds[i].roundness = atoi(str);
    }

    len = xml_get_attribute_of_element(hdl, "rot", str, MAX_STR_LEN);
    if(len >= 0)
    {
      for(j=0; j<len; j++)
      {
        if((str[j] >= '0') && (str[j] <= '9'))  break;
      }

      pack->smds[i].rotation = atof(str + j);
    }

    if(xml_get_attribute_of_element(hdl, "stop", str, MAX_STR_LEN) >= 0)
    {
      if(!strcmp(str, "yes"))
      {
        pack->smds[i].stop = 1;
      }
      else if(!strcmp(str, "no"))
        {
          pack->smds[i].stop = 0;
        }
        else
        {
          fprintf(stderr, "*** ERROR *** undefined smd stop in package: %s stop: %s (-642)\n", req_name, str);
          return -642;
        }
    }
    else
    {
      pack->smds[i].stop = 1;
    }

    if(xml_get_attribute_of_element(hdl, "thermals", str, MAX_STR_LEN) >= 0)
    {
      if(!strcmp(str, "yes"))
      {
        pack->smds[i].thermals = 1;
      }
      else if(!strcmp(str, "no"))
        {
          pack->smds[i].thermals = 0;
        }
        else
        {
          fprintf(stderr, "*** ERROR *** undefined smd thermals in package: %s thermals: %s (-643)\n", req_name, str);
          return -643;
        }
    }
    else
    {
      pack->smds[i].thermals = 1;
    }

    if(xml_get_attribute_of_element(hdl, "cream", str, MAX_STR_LEN) >= 0)
    {
      if(!strcmp(str, "yes"))
      {
        pack->smds[i].cream = 1;
      }
      else if(!strcmp(str, "no"))
        {
          pack->smds[i].cream = 0;
        }
        else
        {
          fprintf(stderr, "*** ERROR *** undefined smd cream in package: %s cream: %s (-644)\n", req_name, str);
          return -644;
        }
    }
    else
    {
      pack->smds[i].cream = 1;
    }

    pack->smd_cnt++;

    xml_go_up(hdl);
  }

  return 0;
}








