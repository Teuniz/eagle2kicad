/*
***************************************************************************
*
* Author: Teunis van Beelen
*
* Copyright (C) 2021 Teunis van Beelen
*
* Email: teuniz@protonmail.com
*
***************************************************************************
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, version 3 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
***************************************************************************
*/


#include "deviceset.h"


int get_deviceset_data(struct xml_handle *hdl, struct deviceset_struct *dev)
{
  int i, j, k;

  char str[MAX_STR_LEN]="";

  memset(dev, 0, sizeof(struct deviceset_struct));

  if(xml_get_attribute_of_element(hdl, "name", str, MAX_STR_LEN) < 1)
  {
    fprintf(stderr, "*** ERROR *** element \"deviceset\" has no name\n");
    return -1;
  }

  trim_spaces(str);

  if(strlen(str) < 1)
  {
    fprintf(stderr, "*** ERROR *** element \"deviceset\" has no name\n");
    return -1;
  }

  str_replace_substr(str, MAX_STR_LEN, -1, " ", "_");

  strlcpy(dev->name, str, DEV_MAX_DAT_LEN);

  if(xml_get_attribute_of_element(hdl, "prefix", str, MAX_STR_LEN) < 1)
  {
    strlcpy(str, "~", MAX_STR_LEN);
  }
  else
  {
    trim_spaces(str);

    if(strlen(str) < 1)
    {
      strlcpy(str, "~", MAX_STR_LEN);
    }
    else
    {
      str_replace_substr(str, MAX_STR_LEN, -1, " ", "_");

      if(!strcmp(str, "IC"))
      {
        strlcpy(str, "U", MAX_STR_LEN);
      }
    }
  }

  strlcpy(dev->prefix, str, DEV_MAX_DAT_LEN);

  if(!xml_goto_nth_element_inside(hdl, "description", 0))
  {
    if(xml_get_content_of_element(hdl, str, MAX_STR_LEN))
    {
      fprintf(stderr, "*** ERROR *** XML error (-3)\n");
      return -3;
    }

    strlcpy(dev->description, str, DEV_MAX_DAT_LEN);

    for(i=0; i<DEV_MAX_DAT_LEN; i++)
    {
      if(dev->description[i] == 0)
      {
        break;
      }

      if((dev->description[i] == '\n') || (dev->description[i] == '\r') || (dev->description[i] == '\t'))
      {
        dev->description[i] = ' ';
      }
    }

    xml_go_up(hdl);
  }

  if(xml_goto_nth_element_inside(hdl, "gates", 0))
  {
    fprintf(stderr, "*** ERROR *** element \"deviceset\" has no element \"gates\"\n");
    return -4;
  }

  for(i=0; i<DEV_MAX_GATES; i++)
  {
    if(xml_goto_nth_element_inside(hdl, "gate", i))
    {
      break;
    }
    else
    {
      if(xml_get_attribute_of_element(hdl, "name", str, MAX_STR_LEN) < 1)
      {
        fprintf(stderr, "*** ERROR *** XML error (-5)\n");
        return -5;
      }

      strlcpy(dev->gates[i].name, str, DEV_MAX_DAT_LEN);

      if(xml_get_attribute_of_element(hdl, "symbol", str, MAX_STR_LEN) < 1)
      {
        fprintf(stderr, "*** ERROR *** XML error (-6)\n");
        return -6;
      }

      strlcpy(dev->gates[i].symbol, str, DEV_MAX_DAT_LEN);

      dev->gate_cnt++;
    }

    xml_go_up(hdl);
  }

  xml_go_up(hdl);

  if(xml_goto_nth_element_inside(hdl, "devices", 0))
  {
    fprintf(stderr, "*** ERROR *** element \"deviceset\" has no element \"devices\"\n");
    return -7;
  }

  for(i=0; i<DEV_MAX_DEVICES; i++)
  {
    if(xml_goto_nth_element_inside(hdl, "device", i))
    {
      break;
    }
    else
    {
      if(xml_get_attribute_of_element(hdl, "name", str, MAX_STR_LEN) < 0)
      {
        fprintf(stderr, "*** ERROR *** XML error (-7)\n");
        return -7;
      }

      strlcpy(dev->devices[i].name, str, DEV_MAX_DAT_LEN);

      if(xml_get_attribute_of_element(hdl, "package", str, MAX_STR_LEN) > 0)
      {
        strlcpy(dev->devices[i].package, str, DEV_MAX_DAT_LEN);
      }

      if(!xml_goto_nth_element_inside(hdl, "connects", 0))
      {
        for(j=0; j<DEV_MAX_CONNECTS; j++)
        {
          if(xml_goto_nth_element_inside(hdl, "connect", j))
          {
            break;
          }
          else
          {
            if(xml_get_attribute_of_element(hdl, "gate", str, MAX_STR_LEN) < 0)
            {
              fprintf(stderr, "*** ERROR *** XML error (-10)\n");
              return -10;
            }

            strlcpy(dev->devices[i].connects[j].gate, str, DEV_MAX_DAT_LEN);

            if(xml_get_attribute_of_element(hdl, "pin", str, MAX_STR_LEN) < 0)
            {
              fprintf(stderr, "*** ERROR *** XML error (-11)\n");
              return -11;
            }

            strlcpy(dev->devices[i].connects[j].pin, str, DEV_MAX_DAT_LEN);

            if(xml_get_attribute_of_element(hdl, "pad", str, MAX_STR_LEN) < 0)
            {
              fprintf(stderr, "*** ERROR *** XML error (-12)\n");
              return -12;
            }

            strlcpy(dev->devices[i].connects[j].pad, str, DEV_MAX_DAT_LEN);

            xml_go_up(hdl);

            dev->devices[i].connect_cnt++;

  //          printf("%s   %s   %s\n", dev->devices[i].connects[j].gate, dev->devices[i].connects[j].pin, dev->devices[i].connects[j].pad);
          }
        }

        xml_go_up(hdl);
      }

      if(xml_goto_nth_element_inside(hdl, "technologies", 0))
      {
        fprintf(stderr, "*** ERROR *** element \"device\" has no element \"technologies\"\n");
        return -13;
      }

      for(j=0; j<DEV_MAX_TECHNOLOGIES; j++)
      {
//        xml_get_name_of_element(hdl, str, MAX_STR_LEN);  printf("A element name: %s   j: %i\n", str, j);

        if(xml_goto_nth_element_inside(hdl, "technology", j))
        {
          break;
        }
        else
        {
          if(xml_get_attribute_of_element(hdl, "name", str, MAX_STR_LEN) < 0)
          {
            fprintf(stderr, "*** ERROR *** XML error (-14)\n");
            return -14;
          }

          strlcpy(dev->devices[i].technologies[j].name, str, DEV_MAX_DAT_LEN);

          for(k=0; k<DEV_MAX_ATTRIBUTES; k++)
          {
            if(xml_goto_nth_element_inside(hdl, "attribute", k))
            {
              break;
            }
            else
            {
              if(xml_get_attribute_of_element(hdl, "name", str, MAX_STR_LEN) < 0)
              {
                fprintf(stderr, "*** ERROR *** XML error (-15)\n");
                return -15;
              }

              strlcpy(dev->devices[i].technologies[j].attributes[k].name, str, DEV_MAX_DAT_LEN);

              if(xml_get_attribute_of_element(hdl, "value", str, MAX_STR_LEN) < 0)
              {
                fprintf(stderr, "*** ERROR *** XML error (-16)\n");
                return -16;
              }

              strlcpy(dev->devices[i].technologies[j].attributes[k].value, str, DEV_MAX_DAT_LEN);

//              printf("processed attribute: %i\n", dev->devices[i].technologies[j].attribute_cnt);

              xml_go_up(hdl);

              dev->devices[i].technologies[j].attribute_cnt++;

//              printf("attribute name: %s     value: %s\n", dev->devices[i].technologies[j].attributes[k].name, dev->devices[i].technologies[j].attributes[k].value);
            }
          }

          xml_go_up(hdl);

//          printf("processed technology: %i\n", dev->devices[i].technology_cnt);

          dev->devices[i].technology_cnt++;
        }
      }

      xml_go_up(hdl);

//      printf("processed device: %i\n", dev->device_cnt);

      dev->device_cnt++;
    }

    xml_go_up(hdl);
  }

  return 0;
}













